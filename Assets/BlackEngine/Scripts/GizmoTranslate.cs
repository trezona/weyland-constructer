﻿// using UnityEngine;
// using UnityEngine.ProBuilder;
// using BlackEngine.MeshEditor;

// namespace BlackEngine
// {
//     public class GizmoTranslate : CoreEngine
//     {
//         public Transform targetObject;
//         public LayerMask layerMask;

//         Transform[] xAxisObject = new Transform[4];
//         Transform[] yAxisObject = new Transform[4];
//         Transform[] zAxisObject = new Transform[4];
//         Transform hitObject;
//         Vector3 hitLocalPoint = Vector3.zero;
//         Color _color;

//         //PreStart init Settings
//         void Awake()
//         {
//             for (int i = 0; i < transform.childCount; i++)
//             {
//                 for (int b = 0; b < transform.GetChild(i).childCount; b++)
//                 {
//                     switch (i)
//                     {
//                         case 0: xAxisObject[b] = transform.GetChild(i).GetChild(b); break;
//                         case 1: yAxisObject[b] = transform.GetChild(i).GetChild(b); break;
//                         case 2: zAxisObject[b] = transform.GetChild(i).GetChild(b); break;
//                     }
//                 }
//             }
//         }
//         public void UpdateGizmo(Transform transform)
//         {
//             this.transform.position = transform.position;
//             this.transform.rotation = transform.rotation;
//         }
//         void Update()
//         {
//             if (!targetObject) return;
//             Ray ray = camera.ScreenPointToCenter();
//             RaycastHit hit;
//             float distance = Vector3.Distance(Camera.main.transform.position, targetObject.transform.position);

//             if (Physics.Raycast(ray, out hit, distance * 2, layerMask) && hitObject == null)
//             {
//                 hitObject = hit.transform;
//                 _color = hitObject.GetComponent<Renderer>().material.color;
//                 hitObject.GetComponent<Renderer>().material.color = Color.white;
//             }
//             else if (hitObject != null && !Input.GetMouseButton(0))
//             {
//                 hitObject.GetComponent<Renderer>().material.color = _color;
//                 hitObject = null;
//             }

//             if (Input.GetMouseButtonDown(0)) hitLocalPoint = targetObject.transform.InverseTransformPoint(ray.GetPoint(distance));
//             if (Input.GetMouseButton(0) && hitObject != null)
//             {
//                 Vector3 offset = targetObject.transform.InverseTransformPoint(ray.GetPoint(distance));
//                 Vector3 moveTo = Vector3.zero;
//                 switch (hitObject.parent.name)
//                 {
//                     case "x_axis":
//                         if (hitObject.transform.name == "x_plane")
//                         {
//                             moveTo = new Vector3(0, offset.y - hitLocalPoint.y, offset.z - hitLocalPoint.z);
//                             break;
//                         }
//                         moveTo = new Vector3(offset.x - hitLocalPoint.x, 0.0f, 0.0f);
//                         break;
//                     case "y_axis":
//                         if (hitObject.transform.name == "y_plane")
//                         {
//                             moveTo = new Vector3(offset.x - hitLocalPoint.x, 0, offset.z - hitLocalPoint.z);
//                             break;
//                         }
//                         moveTo = new Vector3(0, offset.y - hitLocalPoint.y, 0);
//                         break;
//                     case "z_axis":
//                         if (hitObject.transform.name == "z_plane")
//                         {
//                             moveTo = new Vector3(offset.x - hitLocalPoint.x, offset.y - hitLocalPoint.y, 0);
//                             break;
//                         }
//                         moveTo = new Vector3(0, 0, offset.z - hitLocalPoint.z);
//                         break;
//                 }
//                 //targetObject.transform.Translate(Snapping.SnapValue(moveTo, ProBuilderSettings.SnappingValue));
//                 UpdateGizmo(targetObject);
//             }
//         }
//     }
// }