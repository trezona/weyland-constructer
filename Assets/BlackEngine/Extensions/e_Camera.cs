﻿using UnityEngine;

public static class e_Camera
{
    public static Ray ScreenPointToCenter(this Camera camera) => camera.ScreenPointToRay(Input.mousePosition);
}
