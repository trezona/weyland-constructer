﻿using System;
using Unity.Mathematics;
using UnityEngine;
using Weyland.InputSystem;
using UnityEngine.InputSystem;
using Weyland;

namespace BlackEngine.CharacterController
{
    public class PlayerInputControl : CoreEngine
    {
        public PlayerController Character;
        public FollowCamera CharacterCamera;

        private void Awake()
        {   
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            transform.parent.position = Weyland.PlayerData.Read<float3>("BlackEngine.CharacterController.playerPosition", Vector3.up);
            camera.transform.rotation = Quaternion.Euler(Weyland.PlayerData.Read<float3>("BlackEngine.CharacterController.playerRotation", Vector3.zero));
        }

        private void Start()
        {
            InputManager.Player.Enable();
            InputManager.Player.Jump.canceled += delegate (InputAction.CallbackContext context) {  characterInputs.Jump = false; Character.ApplyInputs(ref characterInputs); };
            InputManager.Player.Jump.started += delegate (InputAction.CallbackContext context) { characterInputs.Jump = true; Character.ApplyInputs(ref characterInputs); };
            InputManager.Player.Move.performed += delegate(InputAction.CallbackContext context) 
            { 
                var look = context.ReadValue<Vector2>();
                characterInputs.MoveAxisForward = look.y;
                characterInputs.MoveAxisRight = look.x;
                Character.ApplyInputs(ref characterInputs);
            };

        }

        private void Update()
        {
            CharacterCamera.UpdateControl();
            HandleCharacterInput();

            if(Weyland.PlayerData.Read(Weyland.GameSettings.OptionsKey.SavePlayerPosition, false))
            {
                Weyland.PlayerData.Write("BlackEngine.CharacterController.playerPosition", (float3)transform.parent.position);
                Weyland.PlayerData.Write("BlackEngine.CharacterController.playerRotation", (float3)camera.transform.rotation.eulerAngles);
            }

        }
            
        PlayerCharacterInputs characterInputs = new PlayerCharacterInputs();
        private void HandleCharacterInput()
        {
            if (Cursor.lockState == CursorLockMode.Locked)
            {
                // Build the CharacterInputs struct
                characterInputs.CameraRotation = Camera.main.transform.rotation;//CharacterCamera.Transform.rotation;
                characterInputs.SprintHold = Keyboard.current.leftShiftKey.isPressed;
                //Jump
                //Crouch
                characterInputs.CrouchUp = Keyboard.current.cKey.wasReleasedThisFrame;
                characterInputs.CrouchDown = Keyboard.current.cKey.wasPressedThisFrame;
                characterInputs.CrouchHold = Keyboard.current.cKey.isPressed;
            }
            // Apply inputs to character
            Character.ApplyInputs(ref characterInputs);
        }
    }
}
