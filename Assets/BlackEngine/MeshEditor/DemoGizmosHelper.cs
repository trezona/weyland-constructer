﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoGizmosHelper : MonoBehaviour
{
    LineRenderer lineX, lineY, lineZ;
    private void Start()
    {
        (lineX = new GameObject().AddComponent<LineRenderer>()).transform.parent = transform;
        (lineY = new GameObject().AddComponent<LineRenderer>()).transform.parent = transform;
        (lineZ = new GameObject().AddComponent<LineRenderer>()).transform.parent = transform;
    }

    private void FixedUpdate()
    {
        
    }

}
