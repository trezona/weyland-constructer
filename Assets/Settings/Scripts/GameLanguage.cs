﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class GameLanguage : BaseQualitySettings
    {
        public SystemLanguage defaultLanguage = SystemLanguage.English;
        public int currentIndex { get; private set; }
        public SystemLanguage language
        {
            get => PlayerData.Read("Weyland.Languages.defaultLanguage", defaultLanguage);
            set => PlayerData.Write("Weyland.Languages.defaultLanguage", value);
        }

        public override void Init()
        {
            try
            {
                nameText = Language.Read(OptionsKey.Language, "Language");
                valueText = language.ToString();
                currentIndex = Language.instance.languageSupport.FindIndex(u => u == (int)language);
            }
            catch (Exception e) { Log.Write(e); }
        }

        public override void Next()
        {
            currentIndex = (currentIndex + 1) % Language.AllLanguages().Length;
            valueText = (language = (SystemLanguage)Language.AllLanguages()[currentIndex]).ToString();
        }
        public override void Prev()
        {
            if (currentIndex != 0) currentIndex = (currentIndex - 1) % Language.AllLanguages().Length;
            else currentIndex = (Language.AllLanguages().Length - 1);

            valueText = (language = (SystemLanguage)Language.AllLanguages()[currentIndex]).ToString();
        }
    }
}
