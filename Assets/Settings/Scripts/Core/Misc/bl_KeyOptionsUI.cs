﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using Weyland.InputSystem;
using UnityEngine.InputSystem;
using Weyland;

public class bl_KeyOptionsUI : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private bool DetectIfKeyIsUse = true;
    [Header("References")]
    [SerializeField] private GameObject KeyOptionPrefab;
    [SerializeField] private Transform KeyOptionPanel;
    [SerializeField] private GameObject WaitKeyWindowUI;
    [SerializeField] private Text WaitKeyText;

    private bool WaitForKey = false;
    private bl_KeyInfo WaitFunctionKey;
    private List<bl_KeyInfoUI> cacheKeysInfoUI = new List<bl_KeyInfoUI>();

    void Start()
    {
        InstanceKeysUI();
        WaitKeyWindowUI.SetActive(false);
    }

    void InstanceKeysUI()
    {
        List<bl_KeyInfo> keys = new List<bl_KeyInfo>();
        keys = bl_Input.Instance.AllKeys;

        for (int i = 0; i < keys.Count; i++)
        {
            GameObject kui = Instantiate(KeyOptionPrefab) as GameObject;
            kui.GetComponent<bl_KeyInfoUI>().Init(keys[i], this);
            kui.transform.SetParent(KeyOptionPanel, false);
            kui.gameObject.name = keys[i].Function;
            cacheKeysInfoUI.Add(kui.GetComponent<bl_KeyInfoUI>());
        }
    }

    void ClearList()
    {
        foreach (bl_KeyInfoUI kui in cacheKeysInfoUI) { Destroy(kui.gameObject); }
        cacheKeysInfoUI.Clear();
    }

    void Update()
    {
        if (WaitForKey == true && m_InterectableKey) { DetectKey(); }
    }

    void Test()
    {
        var m_RebindingOperation = InputManager.Player.Jump.PerformInteractiveRebinding();
        if (m_RebindingOperation == null)
            m_RebindingOperation = new InputActionRebindingExtensions.RebindingOperation();

        m_RebindingOperation
            // .WithExpectedControlLayout(m_ExpectedControlLayout)
            // Require minimum actuation of 0.15f. This is after deadzoning has been applied.
            .WithMagnitudeHavingToBeGreaterThan(0.15f)
            ////REVIEW: should we exclude only the system's active pointing device?
            // With the mouse operating the UI, its cursor control is too fickle a thing to
            // bind to. Ignore mouse position and delta and clicks.
            // NOTE: We go for all types of pointers here, not just mice.
            .WithControlsExcluding("<Pointer>/position")
            .WithControlsExcluding("<Pointer>/delta")
            .WithControlsExcluding("<Pointer>/button")
            .WithControlsExcluding("<Mouse>/leftButton")
            .WithControlsExcluding("<Mouse>/scroll")
            .OnPotentialMatch(
                operation =>
                {
                        // We never really complete the pick but keep listening for as long as the "Interactive"
                        // button is toggled on.
                        // Debug.Log(operation);
                        operation.Complete();
                        // Repaint();
                    })
            .OnCancel(
                operation =>
                {
                    Debug.Log("Cancel");
                        // Repaint();
                    })
            .OnApplyBinding(
                (operation, newPath) =>
                {
                        // InputManager.Player.SetCallbacks((actions) => { actions = operation.selectedControl; });
                        // Debug.Log(operation.selectedControl.path);
                        // operation.action.Enable();

                        // Debug.Log(newPath);
                        // This is never invoked (because we don't complete the pick) but we need it nevertheless
                        // as RebindingOperation requires the callback if we don't supply an action to apply the binding to.
                    });

        // If we have control paths to match, pass them on.
        m_RebindingOperation.WithoutControlsHavingToMatchPath();
        // if (m_ControlPathsToMatch.LengthSafe() > 0) .Select(x => m_RebindingOperation.WithControlsHavingToMatchPath(x));

        m_RebindingOperation.Start();
    }

    void DetectKey()
    {
        foreach (KeyCode vKey in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKey(vKey))
            {
                if (DetectIfKeyIsUse && bl_Input.Instance.isKeyUsed(vKey) && vKey != WaitFunctionKey.Key)
                {
                    WaitKeyText.text = string.Format("KEY <b>'{0}'</b> IS ALREADY USE, \n PLEASE PRESS ANOTHER KEY FOR REPLACE <b>{1}</b>", vKey.ToString().ToUpper(), WaitFunctionKey.Description.ToUpper());
                }
                else
                {
                    KeyDetected(vKey);
                    WaitForKey = false;
                }
            }
        }
    }

    public void SetWaitKeyProcess(bl_KeyInfo info)
    {
        if (WaitForKey) return;
        InputManager.Player.Disable();
        Test();

        WaitFunctionKey = info;
        WaitForKey = true;
        WaitKeyText.text = string.Format("PRESS A KEY FOR REPLACE <b>{0}</b>", info.Description.ToUpper());
        WaitKeyWindowUI.SetActive(true);
    }

    void KeyDetected(KeyCode KeyPressed)
    {
        if (WaitFunctionKey == null) { Debug.LogError("Empty function waiting"); return; }

        if (bl_Input.Instance.SetKey(WaitFunctionKey.Function, KeyPressed))
        {
            ClearList();
            InstanceKeysUI();
            WaitFunctionKey = null;
            WaitKeyWindowUI.SetActive(false);
        }
    }

    public void CancelWait()
    {
        WaitForKey = false;
        WaitFunctionKey = null;
        WaitKeyWindowUI.SetActive(false);
        InteractableKey = true;
    }

    private bool m_InterectableKey = true;
    public bool InteractableKey
    {
        get => m_InterectableKey;
        set => m_InterectableKey = value;
    }
}