﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class SelectableText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Color OnEnterColor = new Color(1,1,1,1);
    public float Duration = 1;

    private Text m_Text;
    private Color defaultColor;
    private Button[] m_Button;
    private bool isHover;
    // private Button m_Button;
    // private ColorBlock defaultColorBlock;
    // private ColorBlock OnSelectColorBlock;

    void Awake()
    {
        try
        {
            m_Text = GetComponentsInChildren<Text>()[1];
            if((m_Button = GetComponentsInChildren<Button>()).Length != 2) Destroy(this);
            defaultColor = m_Text.color;
        }
        catch { Destroy(this); }

    //     if(GetComponent<Button>() != null)
    //     {
    //         m_Button = GetComponent<Button>();
    //         defaultColorBlock = m_Button.colors;
    //         OnSelectColorBlock = defaultColorBlock;
    //         OnSelectColorBlock.normalColor = OnEnterColor;
    //     }
    }

    public void Update()
    {
        if(!isHover) return;
        if(Keyboard.current.leftArrowKey.wasPressedThisFrame) m_Button[0].onClick.Invoke();
        if(Keyboard.current.rightArrowKey.wasPressedThisFrame) m_Button[1].onClick.Invoke();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isHover = true;
        m_Text.CrossFadeColor(OnEnterColor, Duration, true, true);
        // if(m_Button != null) { m_Button.colors = OnSelectColorBlock; }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isHover = false;
        m_Text.CrossFadeColor(defaultColor, Duration, true, true);
        // if (m_Button != null) { m_Button.colors = defaultColorBlock; }
    }
}