﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Weyland;
using System.Collections.Generic;
using System;
using UnityEngine.InputSystem;

namespace Weyland.GameSettings
{
    public class GameOptions : MonoBehaviour
    {
        [SerializeField] private List<GameObject> contentOptions;
        [SerializeField] private List<Button> menuButtons;
        [SerializeField] private Animator optionsAnimator;
        [SerializeField] private Animator contentAnimator;

        // [SerializeField] private Slider HUDScaleFactor;
        // [SerializeField] private Text HudScaleText;

        // [SerializeField] private Text PauseText;
        // private bool _isPauseSound;

        // [SerializeField] private Text VolumenText;
        // [SerializeField] private Slider VolumenSlider;
        // [SerializeField] private Text TitlePanelText;
        // [SerializeField] private CanvasScaler HUDCanvas;
        // private float _hudScale;

        private GameObject _rootPanel;
        public static GameOptions Instance { get; private set; }
        void Awake()
        {
            Instance = this;
            _rootPanel = transform.GetChild(0).gameObject;
            // if (HUDCanvas) { _hudScale = (1 - HUDCanvas.matchWidthOrHeight); }
        }

        private void Start()
        {
            ChangeWindow(0, false);
            SetActiveButton(menuButtons[0]);
            _rootPanel.SetActive(false);
        }

        public void SaveOptions() => PlayerData.Save();
        public void ChangeWindow(int id) => ChangeWindow(id, true);
        public void ChangeWindow(int id, bool isAnim)
        {
            if (isAnim) optionsAnimator.Play("Change", 0, 0);
            StartCoroutine(WaitForSwichet(id));
        }
        public void SetActiveButton(Button b)
        {
            menuButtons.ForEach(button => button.interactable = true);
            b.interactable = false;
        }

        private bool _isAnimHide = true;
        private bool _isShow = false;
        public static bool showWindow
        {
            get => Instance._isShow;
            set
            {

                Instance._isShow = value;
                if (value)
                {
                    Instance.StopCoroutine("HideAnimate");
                    Instance._rootPanel.SetActive(true);
                    Instance.contentAnimator.SetBool("Show", true);
                }
                else
                {
                    if (Instance._isAnimHide) Instance.StartCoroutine("HideAnimate");
                    else Instance._rootPanel.SetActive(false);
                }
            }
        }


        // public void PauseSound(bool b)
        // {
        //     _isPauseSound = b;
        //     string t = (_isPauseSound) ? "ON" : "OFF";
        //     PauseText.text = t;
        // }

        // public void SetHUDScale(float value)
        // {
        //     if (HUDCanvas == null) return;

        //     HUDCanvas.matchWidthOrHeight = (1 - value);
        //     _hudScale = value;
        //     HudScaleText.text = string.Format("{0}", value.ToString("F2"));
        // }

        // private float _volumen;
        // public void Volumen(float v)
        // {
        //     AudioListener.volume = v;
        //     _volumen = v;
        //     VolumenText.text = (_volumen * 100).ToString("00") + "%";
        // }

        private IEnumerator WaitForSwichet(int _id)
        {
            yield return StartCoroutine(WaitForRealSeconds(0.25f));
            contentOptions.ForEach(panel => panel.SetActive(false));
            contentOptions[_id].SetActive(true);
            // if (TitlePanelText != null) TitlePanelText.text = contentOptions[_id].name.ToUpper();
        }
        private static IEnumerator WaitForRealSeconds(float time)
        {
            float start = Time.realtimeSinceStartup;
            while (Time.realtimeSinceStartup < start + time) yield return null;
        }
        private IEnumerator HideAnimate()
        {
            if (contentAnimator != null)
            {
                contentAnimator.SetBool("Show", false);
                yield return new WaitForSeconds(contentAnimator.GetCurrentAnimatorStateInfo(0).length);
                _rootPanel.SetActive(false);
            }
            else _rootPanel.SetActive(false);
        }

    }
}