using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

namespace Weyland.GameSettings
{
    [Serializable]
    public class BaseQualitySettings : MonoBehaviour
    {
        protected Text[] qualityText;
        protected string nameText { get => qualityText[0].text; set => qualityText[0].text = value; }
        protected string valueText { get => qualityText[1].text; set => qualityText[1].text = value; }

        private Button[] _button;
        private void Start() => Init();


        private void Awake()
        {
            try
            {
                qualityText = gameObject.GetComponentsInChildren<Text>();

                if ((_button = GetComponentsInChildren<Button>()).Length != 2) Destroy(this);
                _button[0].onClick.RemoveAllListeners();
                _button[0].onClick.AddListener(Prev);
                _button[1].onClick.RemoveAllListeners();
                _button[1].onClick.AddListener(Next);
            } catch(Exception e) { Log.Write(e); }
            PreInit();
        }

        public virtual void PreInit() {}
        public virtual void Init() {}
        public virtual void Prev() {}
        public virtual void Next() {}
    }
}