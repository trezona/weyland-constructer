using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoFullScreen : BaseQualitySettings
    {
        public bool fullScreen
        {
            get => PlayerData.Read(OptionsKey.ResolutionMode, false);
            set 
            {
                PlayerData.Write(OptionsKey.ResolutionMode, value);
                valueText = value ? "ON" : "OFF";
            }
        }
        public override void Prev() => fullScreen = false;
        public override void Next () => fullScreen = true;

        public override void PreInit()
        {
            nameText = Language.Read(OptionsKey.ResolutionMode, "FullScreen");
            valueText = fullScreen ? "ON" : "OFF";
        }
    }
}
