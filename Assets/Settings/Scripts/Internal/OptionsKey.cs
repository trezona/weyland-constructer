﻿namespace Weyland.GameSettings
{
    public static class OptionsKey
    {
        public const string Quality = "Weyland.GameSettings.VideoQuality.quality";
        public const string Resolution = "Weyland.GameSettings.VideoResolution.resolution";
        public const string ResolutionMode = "Weyland.GameSettings.VideoFullScreen.fullScreen";
        public const string AnisoTropic = "Weyland.GameSettings.VideoAnisotropic.anisotropic";
        public const string AntiAliasing = "Weyland.GameSettings.VideoAliasing.aliasing";
        public const string VsyncCount = "Weyland.GameSettings.VideoVsyncCount.vsyncCount";
        public const string BlendWeight = "Weyland.GameSettings.VideoBlendWeights.blendWeight";
        public const string TextureLimit = "Weyland.GameSettings.VideoTextureLimit.textureLimit";
        public const string RealtimeReflection = "Weyland.GameSettings.VideoRealtimeReflection.realtimeReflection";
        public const string ShadowCascade = "Weyland.GameSettings.VideoShadowCascade.shadowCascade";
        public const string ShadowEnable = "Weyland.GameSettings.VideoShadowEnable.shadowEnable";
        public const string ShadownProjection = "Weyland.GameSettings.VideoShadowProjection.shadowProjection";
        public const string ShadowDistance = "Weyland.GameSettings.VideoShadowDistance.shadowDistance";
        public const string LodBias = "Weyland.GameSettings.VideoLodBias.lodBias";
        public const string Brightness = "Weyland.GameSettings.VideoBrightness.brightness";
        public const string ShowFPS = "Weyland.GameSettings.GameShowFPS.showFPS";
        public const string Language = "Weyland.GameSettings.GameLanguage.language";
        public const string SavePlayerPosition = "Weyland.GameSettings.GamePlayerPosition.savePlayerPosition";
        public const string SaveScene = "Weyland.GameSettings.GameSaveScene.saveScene";
        public const string MouseSensity = "Weyland.GameSettings.ControlMouseSensity.mouseSensity";
        public const string MouseSmooth = "Weyland.GameSettings.ControlMouseSensity.mouseSmooth";


        public const string Volumen = "Weyland.Settings.Volumen";
        public const string PauseAudio = "Weyland.Settings.PauseAudio";
        public const string HUDScale = "Weyland.Settings.HudScale";
    }
}