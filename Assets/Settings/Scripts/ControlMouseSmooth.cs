using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class ControlMouseSmooth : MonoBehaviour
    {
        public float defaultMouseSmooth = 2f;
        public float mouseSmooth
        {
            get => PlayerData.Read(OptionsKey.MouseSmooth, defaultMouseSmooth);
            set 
            {
                PlayerData.Write(OptionsKey.MouseSmooth, value);
                valueText = $"{value.ToString("F2")}";
            }
        }

        private Text[] qualityText;
        private string nameText { get => qualityText[0].text; set => qualityText[0].text = value; }
        private string valueText { get => qualityText[1].text; set => qualityText[1].text = value; }
        private Slider distanceSlider;

        public void Awake()
        {
            qualityText = gameObject.GetComponentsInChildren<Text>();
            distanceSlider = gameObject.GetComponentInChildren<Slider>();
            distanceSlider.minValue = 0.01f;
            distanceSlider.maxValue = 2;
            nameText = Language.Read(OptionsKey.MouseSmooth, "Mouse Smooth");
            distanceSlider.onValueChanged.AddListener((float value) => { mouseSmooth = value; });
            distanceSlider.value = mouseSmooth;
            valueText = $"{mouseSmooth.ToString("F2")}";
        }
    }
}
