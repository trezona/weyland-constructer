﻿using UnityEngine;
using UnityEngine.UI;

public class FPS : MonoBehaviour
{
    float deltaTime = 0.0f;
    private Text _fpsText;
    void Awake()
    {
        _fpsText = GetComponent<Text>();
        if(!_fpsText) Destroy(gameObject);
    }
    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} FPS)", msec, fps);
        _fpsText.text = text.ToUpper();
    }
}