using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class ControlMouseSensity : MonoBehaviour
    {
        public float defaultMouseSensity = 0.2f;
        public float mouseSensity
        {
            get => PlayerData.Read(OptionsKey.MouseSensity, defaultMouseSensity);
            set 
            {
                PlayerData.Write(OptionsKey.MouseSensity, value);
                valueText = $"{value.ToString("F2")}";
            }
        }

        private Text[] qualityText;
        private string nameText { get => qualityText[0].text; set => qualityText[0].text = value; }
        private string valueText { get => qualityText[1].text; set => qualityText[1].text = value; }
        private Slider distanceSlider;

        public void Awake()
        {
            qualityText = gameObject.GetComponentsInChildren<Text>();
            distanceSlider = gameObject.GetComponentInChildren<Slider>();
            distanceSlider.minValue = -2;
            distanceSlider.maxValue = 2;
            nameText = Language.Read(OptionsKey.MouseSensity, "Mouse Sensity");
            distanceSlider.onValueChanged.AddListener((float value) => { mouseSensity = value; });
            distanceSlider.value = mouseSensity;
            valueText = $"{mouseSensity.ToString("F2")}";
        }
    }
}
