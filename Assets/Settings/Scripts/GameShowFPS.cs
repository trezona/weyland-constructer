﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class GameShowFPS : BaseQualitySettings
    {
        public GameObject FPSObject;
        public bool showFPS 
        { 
            get => PlayerData.Read(OptionsKey.ShowFPS, true); 
            set => EnableFPS(PlayerData.Write(OptionsKey.ShowFPS, value));
        }

        public override void Init()
        {
            nameText = Language.Read(OptionsKey.ShowFPS, "Show FPS");
            EnableFPS(showFPS);
        }

        public override void Prev() => showFPS = false;
        public override void Next() => showFPS = true;
        public void EnableFPS(bool value)
        {
            valueText = value ? "ON" : "OFF";
            if (FPSObject) FPSObject.SetActive(value);
        }
    }
}
