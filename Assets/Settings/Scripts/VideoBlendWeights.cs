using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Weyland.Languages;

namespace Weyland.GameSettings
{
    [Serializable]
    public class VideoBlendWeights : BaseQualitySettings
    {
        [Range(0, 2)]
        public int defaultBlendWeight = 1;
        public int blendWeight
        {
            get => PlayerData.Read(OptionsKey.BlendWeight, defaultBlendWeight);
            set => PlayerData.Write(OptionsKey.BlendWeight, value);
        }

        public override void PreInit()
        {
            base.PreInit();
            nameText = Language.Read(OptionsKey.BlendWeight, "Blend Weight");
            ChangeBlendWeights();
        }

        public override void Prev() => SetBlendWeights(false);
        public override void Next() => SetBlendWeights(true);

        public void SetBlendWeights(bool mas)
        {
            if (mas) blendWeight = (blendWeight + 1) % 3;
            else if (blendWeight != 0) blendWeight = (blendWeight = -1) % 3;
            else blendWeight = 2;
            ChangeBlendWeights();
        }

        private void ChangeBlendWeights()
        {
            switch (blendWeight)
            {
                case 0:
                    QualitySettings.skinWeights = SkinWeights.OneBone;
                    valueText = SkinWeights.OneBone.ToString().ToUpper();
                    break;
                case 1:
                    QualitySettings.skinWeights = SkinWeights.TwoBones;
                    valueText = SkinWeights.TwoBones.ToString().ToUpper();
                    break;
                case 2:
                    QualitySettings.skinWeights = SkinWeights.FourBones;
                    valueText = SkinWeights.FourBones.ToString().ToUpper();
                    break;
            }
        }
    }
}
