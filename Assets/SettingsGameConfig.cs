﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsGameConfig : MonoBehaviour
{
    public static void SetQuality(int qualityIndex) => QualitySettings.SetQualityLevel(qualityIndex);
}
