﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weyland
{
    public class ToolDelete : IStartup
    {
        public void Dispose()
        {
            BuilderConfigure.title = null;
        }

        public void FixedUpdate()
        {
        }

        public void Init()
        {
            BuilderConfigure.title = "Delete Tool";
        }

        public void OnDisable()
        {
        }

        public void OnEnable()
        {
        }

        public void Update()
        {
            if(InputManager.mouseLeftPressed && ObjectManager.currentSelection != null)
            {
                Transform _transform = ObjectManager.currentSelection;
                ObjectManager.instance.objectManagerFind.Remove(_transform);
                GameObject.Destroy(_transform.gameObject);
            }
        }
    }
}
