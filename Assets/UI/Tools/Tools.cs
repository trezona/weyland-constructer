﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackEngine{
public class Tools : CoreEngine, ITools
{
    public string _nameTool;
    public Texture2D _iconTool;
    public Transform _prefabTool;
    public Transform _camera;
    public string nameTool { get => _nameTool; set => _nameTool = value; }
    
    private Transform m_Object;
    public void Start()
    {
        m_Object = Instantiate(_prefabTool, _camera);
    }
}
}