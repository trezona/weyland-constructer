using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Unity.Mathematics;
using UnityEngine;

namespace Weyland
{
    public static class PlayerData
    {
        public static bool autoSave { get; set; } 
        public static bool isDataLoaded { get; private set; }
        private static List<ConfigData> dataKeys = new List<ConfigData>();

        public static T Read<T>(string key, T defaultValue) 
        {
            int index;
            CheckData();
            if((index = dataKeys.FindIndex(u => u.key == key)) == -1) return defaultValue;
            return (T)dataKeys[index].data;
        }
        public static T Write<T>(string key, T value)
        {
            int index;
            CheckData();
            if((index = dataKeys.FindIndex(u => u.key == key)) != -1) dataKeys[index].data = value;
            else dataKeys.Add(new ConfigData { key = key, data = value });
            if(autoSave) Save();
            return value;
        }

        private static void CheckData()
        {
            if(isDataLoaded) return;
            isDataLoaded = LoadPlayerData();
            // return data;
        }
        #region File Operations
        private static bool LoadPlayerData()
        {
            string path = $"{Application.dataPath}/Data/Configs/Default.data";
            if (!File.Exists(path)) return false;
            
            // IF EXIST, READ
            using (FileStream fstream = File.OpenRead(path))
            {
                dataKeys = (List<ConfigData>)new BinaryFormatter().Deserialize(fstream);
                fstream.Close();
            }
            return true;
        }
        public static bool Save()
        {
            string path = $"{Application.dataPath}/Data/Configs";
            Directory.CreateDirectory(path);

            using (FileStream fstream = new FileStream(path + "/Default.data", FileMode.OpenOrCreate))
            {
                new BinaryFormatter().Serialize(fstream, dataKeys);
                fstream.Close();
            }
            return true;
        }
        #endregion

        // Scheme Table Language
        [Serializable]
        public class ConfigData
        {
            public string key;
            public object data;
        }
    }
}