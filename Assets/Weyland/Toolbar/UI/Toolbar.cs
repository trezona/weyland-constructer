﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Weyland;
using Weyland.InputSystem;
using Weyland.InventorySystem;
using UnityEngine.InputSystem;

namespace Weyland
{
    [DisallowMultipleComponent]
    public class Toolbar : MonoBehaviour
    {
        private void DigitalKeyPressed(InputAction.CallbackContext context)
        {
            if(BuilderConfigure.showWindow) return;
            int value = (int)context.ReadValue<float>();
            if (Keyboard.current.leftCtrlKey.isPressed && value >= 0) this[Mathf.Clamp(value, 0, 9)] = InventoryElement.inventoryElement;
            else activeElementID = value;
        }

        private ToolbarElement[] _toolbarElements = new ToolbarElement[10];
        private Transform _transform;

        private void Awake()
        {
            for (int i = 1; i < _toolbarElements.Length + 1; i++)
            {
                _transform = Instantiate(toolbarItemPrefab, toolbarContent).transform;
                _transform.SetParent(toolbarContent);
                _transform.GetComponentInChildren<Text>().text = (i != 10 ? i.ToString() : "0");
                _transform.gameObject.name = $"Key_{(i != 10 ? i.ToString() : "0")}";

                //Setup
                ToolbarElement item = new ToolbarElement();
                item.activeObject = _transform.Find("Active").gameObject;
                item.imageObject = _transform.GetComponentInChildren<RawImage>();

                _toolbarElements[(i != 10 ? i : 0)] = item;
            }
            activeElementID = -1;
        }

        private void Start()
        {
            InputManager.Toolbar.Enable();
            InputManager.Toolbar.digitalKey.performed += DigitalKeyPressed;
        }

        private int _activeElementID;
        public int activeElementID
        {
            get => _activeElementID;
            set
            {
                for (int i = 0; i < _toolbarElements.Length; i++)
                {
                    _toolbarElements[i].activeObject.SetActive(i == (_activeElementID = value));
                    if (i == _activeElementID) InventoryElement.ItemClicked(this[i] != null ? this[i] : null);
                }
            }
        }

        public InventoryElementAsset this[int ID]
        {
            get => _toolbarElements[ID].inventoryElement;
            set
            {
                Debug.Log(InventoryElement.inventoryElement);
                if(!InventoryElement.inventoryElement) return;
                _toolbarElements[ID].inventoryElement = value;
                _toolbarElements[ID].imageObject.texture = value.textureImage;
            }
        }

        public Transform toolbarContent;
        public GameObject toolbarItemPrefab;
    }
}