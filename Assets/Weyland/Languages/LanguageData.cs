using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Weyland.Languages
{
    [Serializable, CreateAssetMenu(menuName = "Weyland/Language", fileName = "language")]
    public class LanguageData : ScriptableObject
    {
        public SystemLanguage language;
        public List<DataKeys> dataKeys = new List<DataKeys>();

        [Serializable]
        public struct DataKeys
        {
            public string key;
            public string text;

            public DataKeys(string key, string text)
            {
                this.key = key;
                this.text = text;
            }
            public DataKeys Set(string text)
            {
                this.text = text;
                return this;
            }
        }

        public DataKeys Read(string key)
        {
            int index;
            if((index = dataKeys.FindIndex(u => u.key == key)) > -1) return dataKeys[index];
            return new DataKeys(key, key);
        }
        public DataKeys Read(string key, string text)
        {
            int index;
            if((index = dataKeys.FindIndex(u => u.key == key)) > -1) return dataKeys[index];
            return Append(key, text);
        }
        public DataKeys Append(string key, string text) => IsKey(key) ? Read(key) : Write(key, text);
        public DataKeys Write(string key, string text, bool isSave = true)
        {
            int index = IndexKey(key);
            if(index > -1) return dataKeys[index].Set(text);
            if(isSave) Save();
            return Add(new DataKeys(key, text));
        }
        public bool IsKey(string key) => dataKeys.Exists(u => u.key == key);
        public int IndexKey(string key) => dataKeys.FindIndex(u => u.key == key);
        public void Save()
        {
            string path = $"{Application.dataPath}/Data/Languages/";
            Directory.CreateDirectory(path);

            using (FileStream fstream = new FileStream($"{path}{language.ToString()}.json", FileMode.OpenOrCreate))
            {
                byte[] array = System.Text.Encoding.Default.GetBytes(JsonUtility.ToJson(this, true));
                fstream.Write(array, 0, array.Length);
                fstream.Close();
            }
        }

        private DataKeys Add(DataKeys data) 
        {
            dataKeys.Add(data);
            return data;
        }

        #if UNITY_EDITOR
        public void CreateAsset(string nameAsset) => AssetDatabase.CreateAsset(this, "Assets/Resources/Data/Languages/" + nameAsset + ".asset");
        #endif
    }
}