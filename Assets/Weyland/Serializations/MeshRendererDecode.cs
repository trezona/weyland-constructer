using System;
using System.Collections.Generic;
using System.IO;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.ProBuilder;

namespace Weyland.Serializations
{
    [Serializable]
    public struct MeshRendererDecode
    {
        public MaterialDecode[] materials;
        public MeshRendererDecode(GameObject gameObject) : this(gameObject.GetComponent<MeshRenderer>()) { }
        public MeshRendererDecode(MeshRenderer meshRenderer)
        {
            materials = new MaterialDecode[meshRenderer.sharedMaterials.Length];
            for(int index = 0; index < materials.Length; index++)
            {
                materials[index] = MaterialDecode.Create(meshRenderer.sharedMaterials[index]);
            }
        }
        public string ToJson() => JsonUtility.ToJson(this, true);
        public void Apply(GameObject gameObject) => Apply(gameObject.GetComponent<MeshRenderer>());
        public void Apply(MeshRenderer meshRenderer)
        {
            Material[] materialsArray = new Material[materials.Length];
            for(int index = 0; index < materials.Length; index++)
            {
                materialsArray[index] = materials[index].GenerateMaterial();
            }
            meshRenderer.sharedMaterials = materialsArray;
        }
    }
}