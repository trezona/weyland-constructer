using System;
using Unity.Mathematics;
using UnityEngine;

namespace Weyland.Serializations
{
    [Serializable]
    public struct MaterialDecode
    {
        public string nameMaterial;

        public float4 baseColor;
        public TextureDecode baseColorMap;
        public float metallic;
        public float smoothness;
        public TextureDecode maskMap;
        public TextureDecode normalMap;
        public float normalScale;
        public TextureDecode bentNormalMap;
        public TextureDecode heightMap;
        public float heightOffset;
            
        public Material materialLoaded { get; private set; }
        public bool hasInit { get; private set; }
        
        public Material GenerateMaterial()
        {
            Material material = new Material(Shader.Find("HDRP/Lit"));
            material.name = nameMaterial;
            material.SetVector("_BaseColor", baseColor);
            material.SetTexture("_BaseColorMap", baseColorMap.GenerateTexture());
            material.SetFloat("_Metallic", metallic);
            material.SetFloat("_Smoothness", smoothness);
            material.SetTexture("_MaskMap", maskMap.GenerateTexture());
            material.SetTexture("_NormalMap", normalMap.GenerateTexture());
            material.SetFloat("_NormalScale", normalScale);
            material.SetTexture("_BentNormalMap", bentNormalMap.GenerateTexture());
            material.SetTexture("_HeightMap", heightMap.GenerateTexture());
            material.SetFloat("_HeightOffset", heightOffset);
            return material;
        }

        public static MaterialDecode Create(Material material)
        {
            if(material == null) return Create();
            return new MaterialDecode
            {
                hasInit = true,
                materialLoaded = material,
                nameMaterial = material.name,
                baseColor = material.GetVector("_BaseColor"),
                baseColorMap = TextureDecode.Create(material.GetTexture("_BaseColorMap")),
                metallic = material.GetFloat("_Metallic"),
                smoothness = material.GetFloat("_Smoothness"),
                maskMap = TextureDecode.Create(material.GetTexture("_MaskMap")),
                normalMap = TextureDecode.Create(material.GetTexture("_NormalMap")),
                normalScale = material.GetFloat("_NormalScale"),
                bentNormalMap = TextureDecode.Create(material.GetTexture("_BentNormalMap")),
                heightMap = TextureDecode.Create(material.GetTexture("_HeightMap")),
                heightOffset = material.GetFloat("_HeightOffset")
            };
        }
        public static MaterialDecode Create(string json) => JsonUtility.FromJson<MaterialDecode>(json);
        public static MaterialDecode Create()
        {
            return new MaterialDecode{
                materialLoaded = null,
                hasInit = false,
                nameMaterial = "",
                baseColor = Vector4.one,
                baseColorMap = TextureDecode.Create(),
                metallic = 0,
                smoothness = 0.5f,
                maskMap = TextureDecode.Create(),
                normalMap = TextureDecode.Create(),
                normalScale = 1,
                bentNormalMap = TextureDecode.Create(),
                heightMap = TextureDecode.Create(),
                heightOffset = 0
            };
        }

        public string ToJson() => JsonUtility.ToJson(this, true);
        public static Material[] GetResources() => Resources.LoadAll<Material>("Data/Materials");
    }
}