using UnityEngine;
using UnityEngine.ProBuilder;

namespace Weyland.Serializations
{
    public class GameObjectDecode
    {
        public string name;
        public string geometry;
        public TransformDecode transform;
        public MeshRendererDecode meshRenderer;

        public GameObjectDecode(Transform transform) : this(transform.gameObject) { }
        public GameObjectDecode(GameObject gameObject)
        {
            this.name = gameObject.name;
            transform = new TransformDecode(gameObject);
            meshRenderer = new MeshRendererDecode(gameObject);
            geometry = JsonUtility.ToJson(gameObject.GetComponent<ProBuilderMesh>(), true);
        }
        public string ToJson() => JsonUtility.ToJson(this, true);
        public void Apply(Transform transform) => Apply(transform.gameObject);
        public void Apply(GameObject gameObject)
        {
            gameObject.name = this.name;
            transform.Apply(gameObject);
            meshRenderer.Apply(gameObject);
            JsonUtility.FromJsonOverwrite(geometry, gameObject.GetComponent<ProBuilderMesh>());
        }
        public static GameObjectDecode Create(string json) => JsonUtility.FromJson<GameObjectDecode>(json);
    }
}