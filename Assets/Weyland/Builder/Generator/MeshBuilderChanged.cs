﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using BlackEngine;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.ProBuilder;
using Weyland.InventorySystem;
using Weyland.Serializations;
using BuilderMesh = UnityEngine.ProBuilder.ProBuilderMesh;

namespace Weyland.Builder
{
    public class MeshBuilderChanged : CoreEngine
    {
        public Material previewMaterial;
        public Material defaultMaterial;

        private GameObject _createdObject;

        void Start()
        {
            ReadDirectory();
            Inventory.elementOnChanged.AddListener(MeshOnChanged);
        }

        public void MeshOnChanged(InventoryElementAsset asset)
        {
            if (_createdObject) MonoBehaviour.Destroy(_createdObject);

            // Check is Mesh
            if (asset != null)
            {
                if (asset.categoryName != "Blocks") return;
                BuilderConfigure.title = asset.name;
                switch (asset.name)
                {
                    case "GenerateTorus":
                        (_createdObject = new GameObject("GenerateTorus")).AddComponent<GenerateTorus>();
                        _createdObject.GetComponent<GenerateTorus>().previewMaterial = previewMaterial;
                        _createdObject.GetComponent<GenerateTorus>().defaultMaterial = defaultMaterial;
                        _createdObject.transform.SetParent(this.transform);
                        break;
                    case "GeneratePrism":
                        (_createdObject = new GameObject("GeneratePrism")).AddComponent<GeneratePrism>();
                        _createdObject.GetComponent<GeneratePrism>().previewMaterial = previewMaterial;
                        _createdObject.GetComponent<GeneratePrism>().defaultMaterial = defaultMaterial;
                        _createdObject.transform.SetParent(this.transform);
                        break;
                    case "GeneratePlane":
                        (_createdObject = new GameObject("GeneratePlane")).AddComponent<GeneratePlane>();
                        _createdObject.GetComponent<GeneratePlane>().previewMaterial = previewMaterial;
                        _createdObject.GetComponent<GeneratePlane>().defaultMaterial = defaultMaterial;
                        _createdObject.transform.SetParent(this.transform);
                        break;
                    case "GeneratePipe":
                        (_createdObject = new GameObject("GeneratePipe")).AddComponent<GeneratePipe>();
                        _createdObject.GetComponent<GeneratePipe>().previewMaterial = previewMaterial;
                        _createdObject.GetComponent<GeneratePipe>().defaultMaterial = defaultMaterial;
                        _createdObject.transform.SetParent(this.transform);
                        break;
                    case "GenerateIcosahedron":
                        (_createdObject = new GameObject("GenerateIcosahedron")).AddComponent<GenerateIcosahedron>();
                        _createdObject.GetComponent<GenerateIcosahedron>().previewMaterial = previewMaterial;
                        _createdObject.GetComponent<GenerateIcosahedron>().defaultMaterial = defaultMaterial;
                        _createdObject.transform.SetParent(this.transform);
                        break;
                    case "GenerateDoor":
                        (_createdObject = new GameObject("GenerateDoor")).AddComponent<GenerateDoor>();
                        _createdObject.GetComponent<GenerateDoor>().previewMaterial = previewMaterial;
                        _createdObject.GetComponent<GenerateDoor>().defaultMaterial = defaultMaterial;
                        _createdObject.transform.SetParent(this.transform);
                        break;
                    case "GenerateCylinder":
                        (_createdObject = new GameObject("GenerateCylinder")).AddComponent<GenerateCylinder>();
                        _createdObject.GetComponent<GenerateCylinder>().previewMaterial = previewMaterial;
                        _createdObject.GetComponent<GenerateCylinder>().defaultMaterial = defaultMaterial;
                        _createdObject.transform.SetParent(this.transform);
                        break;
                    case "GenerateCurvedStair":
                        (_createdObject = new GameObject("GenerateCurvedStair")).AddComponent<GenerateCurvedStair>();
                        _createdObject.GetComponent<GenerateCurvedStair>().previewMaterial = previewMaterial;
                        _createdObject.GetComponent<GenerateCurvedStair>().defaultMaterial = defaultMaterial;
                        _createdObject.transform.SetParent(this.transform);
                        break;
                    case "GenerateCone":
                        (_createdObject = new GameObject("GenerateCone")).AddComponent<GenerateCone>();
                        _createdObject.GetComponent<GenerateCone>().previewMaterial = previewMaterial;
                        _createdObject.GetComponent<GenerateCone>().defaultMaterial = defaultMaterial;
                        _createdObject.transform.SetParent(this.transform);
                        break;
                    case "GenerateArch":
                        (_createdObject = new GameObject("GenerateArch")).AddComponent<GenerateArch>();
                        _createdObject.GetComponent<GenerateArch>().previewMaterial = previewMaterial;
                        _createdObject.GetComponent<GenerateArch>().defaultMaterial = defaultMaterial;
                        _createdObject.transform.SetParent(this.transform);
                        break;
                    case "GenerateCube":
                        (_createdObject = new GameObject("GenerateCube")).AddComponent<GenerateCube>();
                        _createdObject.GetComponent<GenerateCube>().previewMaterial = previewMaterial;
                        _createdObject.GetComponent<GenerateCube>().defaultMaterial = defaultMaterial;
                        _createdObject.transform.SetParent(this.transform);
                        break;
                    default:
                        (_createdObject = new GameObject("Custom")).AddComponent<GenerateMeshBehaviour>();
                        _createdObject.transform.SetParent(this.transform);
                        _createdObject.GetComponent<GenerateMeshBehaviour>().previewMaterial = previewMaterial;
                        _createdObject.GetComponent<GenerateMeshBehaviour>().defaultMaterial = defaultMaterial;
                        _createdObject.GetComponent<GenerateMeshBehaviour>().Generate(asset.ContentData<GameObjectDecode>());
                        break;
                }
            }
        }

        public Material test;
        GameObjectDecode gameObjectDecode;
        private GameObjectDecode copyMesh;
        void Update()
        {
            if (!_createdObject) return;
            // CTRL + C, CTRL + V, CTRL + S 
            RaycastHit hit;
            if (Keyboard.current.leftCtrlKey.isPressed && Keyboard.current.cKey.wasPressedThisFrame)
            {
                if (Physics.Raycast(camera.ScreenPointToCenter(), out hit, 10))
                {
                    InventoryElement.ItemClicked(new InventoryElementAsset("Copy Object", "Blocks", copyMesh = new GameObjectDecode(hit.transform)));
                }
            }
            if (Keyboard.current.leftCtrlKey.isPressed && Keyboard.current.sKey.wasPressedThisFrame) SaveModel();
            if (Keyboard.current.leftCtrlKey.isPressed && Keyboard.current.vKey.wasPressedThisFrame)
            {
                InventoryElement.ItemClicked(new InventoryElementAsset("Paste Object", "Blocks", copyMesh));
            }
        }

        [SerializeField] private static List<GameObjectDecode> MeshBuilderArray = new List<GameObjectDecode>();
        private List<GameObjectDecode> ReadDirectory()
        {
            List<GameObjectDecode> _array = new List<GameObjectDecode>();
            try
            {
                if (Directory.Exists($"{Application.dataPath}/Data/Models/"))
                {
                    var files = Directory.GetFiles($"{Application.dataPath}/Data/Models/", "*.json");

                    for (int i = 0; i < files.Length; i++)
                    {
                        string[] fileNameArray;
                        string file = (fileNameArray = files[i].Split('/'))[fileNameArray.Length - 1].Split('.')[0];
                        if (MeshBuilderArray.FindIndex(u => u.name == file) > -1) continue;

                        var prefab = GameObjectDecode.Create(File.ReadAllText(files[i]));
                        _array.Add(prefab);
                        Inventory.CreateCategory("Blocks").CreateElement(new InventoryElementAsset(file, "Blocks", prefab, Texture(file, prefab) as Texture2D));
                    }
                }
            } catch (Exception e) { Log.Write(e); }

            try
            {
                if (Directory.Exists($"{Application.dataPath}/Data/Models/Init/")) 
                {
                    //SETUP OBJECT ON SCENE
                    var files = Directory.GetFiles($"{Application.dataPath}/Data/Models/Init/", "*.json");
                    for (int i = 0; i < files.Length; i++)
                    {
                        string[] fileNameArray;
                        string file = (fileNameArray = files[i].Split('/'))[fileNameArray.Length - 1].Split('.')[0];
                        if (MeshBuilderArray.FindIndex(u => u.name == file) > -1) continue;

                        var prefab = GameObjectDecode.Create(File.ReadAllText(files[i]));
                        GenerateMeshBehaviour.Setup(prefab);
                    }
                }
            }
            catch (Exception e) { Log.Write(e); }

            MeshBuilderArray.AddRange(_array);
            return _array;
        }

        private Texture Texture(string fileName, GameObjectDecode prefab)
        {
            Texture2D texture = new Texture2D(1, 1);
            if (File.Exists($"{Application.dataPath}/Data/Models/{fileName}.png"))
            {
                using (FileStream fstream = File.OpenRead($"{Application.dataPath}/Data/Models/{fileName}.png"))
                {
                    byte[] array = new byte[fstream.Length];
                    fstream.Read(array, 0, array.Length);
                    texture.LoadImage(array);
                    return texture;
                }
            }

            var _generateMesh = ProBuilderMesh.Create();
            prefab.Apply(_generateMesh.gameObject);
            _generateMesh.ToMesh();
            _generateMesh.Refresh();

            var generatePreviewEngine = new GeneratePreviewEngine(_generateMesh.transform, defaultMaterial);
            generatePreviewEngine.RenderReady.AddListener(delegate (Texture2D tex)
            {
                File.WriteAllBytes($"{Application.dataPath}/Data/Models/{fileName}.png", tex.EncodeToPNG());
            });
            return generatePreviewEngine.Render();
        }

        private bool SaveModel()
        {
            Directory.CreateDirectory($"{Application.dataPath}/Data/Models/");
            using (FileStream fstream = new FileStream($"{Application.dataPath}/Data/Models/{copyMesh.name}.json", FileMode.OpenOrCreate))
            {
                byte[] array = System.Text.Encoding.Default.GetBytes(copyMesh.ToJson());
                fstream.Write(array, 0, array.Length);
                fstream.Close();
            }
            // copyMesh.GetComponent<MeshRenderer>().sharedMaterials.Save($"Data/Models/{Time.time}", "Material");
            ReadDirectory();
            return true;
        }
    }
}