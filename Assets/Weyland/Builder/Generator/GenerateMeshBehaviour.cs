using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.ProBuilder;
using Weyland.InputSystem;
using Weyland.InventorySystem;
using Weyland.Languages;
using Weyland.Serializations;

namespace Weyland.Builder
{
    public class GenerateMeshBehaviour : CoreEngine
    {
        public Material previewMaterial { get; set; }
        public Material defaultMaterial { get; set; }

        public float snapOffset { get; set; }
        public int distancePlacement { get; set; }

        private Transform _transformTarget;
        private Transform _transformMesh;
        private ProBuilderMesh _builderMesh;

        public virtual void PreInit() { }
        private void Start()
        {
            snapOffset = PlayerData.Read("Weyland.Builder.GenerateMeshBehaviour.snapOffset", 1f);
            distancePlacement = PlayerData.Read("Weyland.Builder.GenerateMeshBehaviour.distancePlacement", 10);

            // UI GENERATE
            BuilderConfigure.Clear();
            BuilderConfigure.Generate(
                Language.Read("Weyland.Builder.GenerateMeshBehaviour.snapOffset"),
                Language.Read("Weyland.Builder.GenerateMeshBehaviour.snapOffset.info"), snapOffset).AddListener(delegate (float value)
                {
                    snapOffset = PlayerData.Write("Weyland.Builder.GenerateMeshBehaviour.snapOffset", Mathf.Clamp(value, 0.1f, 10));
                });


            BuilderConfigure.Generate(
                Language.Read("Weyland.Builder.GenerateMeshBehaviour.distancePlacement"),
                Language.Read("Weyland.Builder.GenerateMeshBehaviour.distancePlacement.info"), distancePlacement).AddListener(delegate (int value)
                {
                    distancePlacement = PlayerData.Write("Weyland.Builder.GenerateMeshBehaviour.distancePlacement", Mathf.Clamp(value, 1, 1000));
                });
            PreInit();
        }
        private void OnDisable() => Destroy(this.gameObject);

        private float3 GetSnapPosition(float3 point) => HandleAlignment.snapActive ? (float3)eSnapping.SnapValue(point, snapOffset) : point;
        private float3 GetLocalPosition(float3 position)
        {
            if (_transformTarget) return _transformTarget.TransformPoint(GetSnapPosition(_transformTarget.InverseTransformPoint(position)));
            return GetSnapPosition(position);
        }

        public static void Setup(GameObjectDecode gameObjectDecode)
        {
            var prefab = ProBuilderMesh.Create();
            gameObjectDecode.Apply(prefab.gameObject);
            prefab.gameObject.AddComponent<MeshCollider>();
            prefab.ToMesh();
            prefab.Refresh();
        }

        public void Setup(ProBuilderMesh builderMesh)
        {
            if(builderMesh == null) return;
            var prefab = ProBuilderMesh.Create(builderMesh.positions, builderMesh.faces);
            prefab.name = builderMesh.name;
            prefab.unwrapParameters = new UnwrapParameters();
            prefab.SetMaterial(prefab.faces, defaultMaterial);
            
            prefab.transform.position = builderMesh.transform.position;
            prefab.transform.rotation = builderMesh.transform.rotation;
            
            prefab.ToMesh();
            prefab.Refresh();

            prefab.gameObject.AddComponent<MeshCollider>();
            ObjectManager.instance.objectManagerFind.Add(prefab.transform);
        }
        public void Setup()
        {
            if (!_builderMesh) return;

            var _generateMesh = ProBuilderMesh.Create(_builderMesh.positions, _builderMesh.faces);
            _generateMesh.name = _builderMesh.name;
            if (_gameObjectDecode != null) _gameObjectDecode.Apply(_generateMesh.gameObject);
            else
            {
                _generateMesh.unwrapParameters = new UnwrapParameters();
                _generateMesh.SetMaterial(_generateMesh.faces, defaultMaterial);
            }
            _generateMesh.transform.position = _transformMesh.position;
            _generateMesh.transform.rotation = _transformMesh.rotation;
            _generateMesh.ToMesh();
            _generateMesh.Refresh();
            _generateMesh.gameObject.AddComponent<MeshCollider>();
            _generateMesh.gameObject.GetComponent<MeshCollider>().isTrigger = false;
            _generateMesh.gameObject.GetComponent<MeshCollider>().convex = false;
            ObjectManager.instance.objectManagerFind.Add(_generateMesh.transform);
        }

        private GameObjectDecode _gameObjectDecode;
        public void Generate(GameObjectDecode gameObjectDecode)
        {
            var _prefab = ProBuilderMesh.Create();
            (_gameObjectDecode = gameObjectDecode).Apply(_prefab.gameObject);
            lockPosition = true;
            // Setup(gameObjectDecode);
            Generate(_prefab);
        }

        public void Generate(ProBuilderMesh generateMesh)
        {
            if (_builderMesh)
            {
                generateMesh.transform.position = _transformMesh.position;
                generateMesh.transform.rotation = _transformMesh.rotation;
                Destroy(_builderMesh.gameObject);
            }
            (_transformMesh = (_builderMesh = generateMesh).transform).SetParent(this.transform);
            generateMesh.unwrapParameters = new UnwrapParameters();
            generateMesh.SetMaterial(generateMesh.faces, previewMaterial);
            generateMesh.ToMesh();
            generateMesh.Refresh();
            generateMesh.gameObject.layer = 2;
            generateMesh.gameObject.AddComponent<MeshCollider>();
            generateMesh.gameObject.GetComponent<MeshCollider>().convex = true;
            generateMesh.gameObject.GetComponent<MeshCollider>().isTrigger = true;
        }

        bool rotate;
        bool lockPosition;

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.R)) rotate = !rotate;
            if (Input.GetKeyDown(KeyCode.L)) lockPosition = !lockPosition;

            if (!_transformMesh) return;
            if (InputManager.activeMouse) return;
            if (!Inventory.showWindow && Mouse.current.leftButton.wasReleasedThisFrame) Setup();

            UpdatePositions();
        }

        private void UpdatePositions()
        {
            RaycastHit _onHit;
            // Setup RAy
            Ray _lineRay = camera.ScreenPointToCenter();
            bool _hasHit = Physics.Raycast(_lineRay, out _onHit, distancePlacement);
            Vector3 _nowPoint = _hasHit ? _onHit.point : _lineRay.GetPoint(distancePlacement);

            // Global
            if (HandleAlignment.AlignmentEnum.Global == HandleAlignment.modeAlignment)
            {
                if (!lockPosition) _transformMesh.position = GetSnapPosition(_nowPoint);
                _transformMesh.rotation = Quaternion.Euler(0, rotate ? camera.transform.rotation.eulerAngles.y : 0, 0);
                return;
            }

            // Local
            if (HandleAlignment.AlignmentEnum.Local == HandleAlignment.modeAlignment)
            {
                if (Mouse.current.rightButton.wasReleasedThisFrame) _transformTarget = _onHit.transform;
                if (!lockPosition) _transformMesh.position = GetLocalPosition(_nowPoint);
                _transformMesh.rotation = Quaternion.Euler(0, rotate ? camera.transform.rotation.eulerAngles.y : 0, 0);
                return;
            }

            // Plane
            if (HandleAlignment.AlignmentEnum.Plane == HandleAlignment.modeAlignment)
            {
                _transformTarget = _onHit.transform;
                if (!lockPosition) _transformMesh.position = GetLocalPosition(_nowPoint + _onHit.normal * 0.1f);
                if (!Mouse.current.rightButton.isPressed)
                {
                    _transformMesh.rotation = _hasHit ?
                        Quaternion.LookRotation(_onHit.normal, _transformTarget.forward == _onHit.normal ? _transformTarget.right : _transformTarget.forward)// .LookAt(Vector3.forward, _rayHit.normal);
                        : Quaternion.identity;
                }
            }
        }


    }
}