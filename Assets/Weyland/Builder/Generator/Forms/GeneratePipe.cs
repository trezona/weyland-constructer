﻿// Copyright (c) 2019 All Rights Reserved
// Dmitriy Annenkov
// 03 18 2019

using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.ProBuilder;
using Weyland;
using Weyland.Builder;
using static UnityEngine.UI.Dropdown;
using Debug = UnityEngine.Debug;
using BuilderMesh = UnityEngine.ProBuilder.ProBuilderMesh;
using Weyland.Serializations;
using Weyland.Languages;

namespace Weyland.Builder
{
    public class GeneratePipe : GenerateMeshBehaviour
    {
        private PivotLocation pivotType = PivotLocation.Center;
        private float radius = 1;
        private float height = 2;
        private float thickness = 0.2f;
        private int subdivAxis = 6;
        private int subdivHeight = 1;

        public override void PreInit()
        {
            string _namespace = this.GetType().FullName;

            pivotType = PlayerData.Read(_namespace + ".pivotType", pivotType);
            radius = PlayerData.Read(_namespace + ".radius", radius);
            height = PlayerData.Read(_namespace + ".height", height);
            thickness = PlayerData.Read(_namespace + ".thickness", thickness);
            subdivAxis = PlayerData.Read(_namespace + ".subdivAxis", subdivAxis);
            subdivHeight = PlayerData.Read(_namespace + ".subdivHeight", subdivHeight);

            GenerateMenu(_namespace);
            MeshGenerate();
        }

        private void GenerateMenu(string _namespace)
        {
            BuilderConfigure.title = Language.Read(_namespace, "Pipe");

            BuilderConfigure.GenerateEnum(
                 Language.Read(_namespace + "Weyland.Builder.pivotType"),
                 Language.Read(_namespace + "Weyland.Builder.pivotType.info"), typeof(PivotLocation), (int)pivotType).AddListener((int value) =>
                 {
                     pivotType = PlayerData.Write(_namespace + ".pivotLocation", (PivotLocation)value);
                     MeshGenerate();
                 });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".radius", "Radius"),
                Language.Read(_namespace + ".radius.info", "Radius of the generated pipe."), radius).AddListener((float value) =>
                {
                    radius = PlayerData.Write(_namespace + ".radius", value);
                    MeshGenerate();
                });


            BuilderConfigure.Generate(
                Language.Read(_namespace + ".height", "Height"),
                Language.Read(_namespace + ".height.info", "Height of the generated pipe."), height).AddListener((float value) =>
                {
                    height = PlayerData.Write(_namespace + ".height", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".thickness", "Thickness"),
                Language.Read(_namespace + ".thickness.info", "How thick the walls will be."), thickness).AddListener((float value) =>
                {
                    thickness = PlayerData.Write(_namespace + ".thickness", value);
                    MeshGenerate();
                });
 
            BuilderConfigure.Generate(
                Language.Read(_namespace + ".subdivAxis", "Subdiv Axis"),
                Language.Read(_namespace + ".subdivAxis.info", "How many subdivisions on the axis."), subdivAxis).AddListener((int value) =>
                {
                    subdivAxis = PlayerData.Write(_namespace + ".subdivAxis", value);
                    MeshGenerate();
                });
 
            BuilderConfigure.Generate(
                Language.Read(_namespace + ".subdivHeight", "Subdiv Height"),
                Language.Read(_namespace + ".subdivHeight.info", "How many subdivisions on the Y axis."), subdivHeight).AddListener((int value) =>
                {
                    subdivHeight = PlayerData.Write(_namespace + ".subdivHeight", value);
                    MeshGenerate();
                });
        }
        private void MeshGenerate() => Generate(ShapeGenerator.GeneratePipe(pivotType, radius, height, thickness, subdivAxis, subdivHeight));
    }
}