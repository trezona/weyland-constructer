﻿// Copyright (c) 2019 All Rights Reserved
// Dmitriy Annenkov
// 03 18 2019

using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.ProBuilder;
using Weyland;
using Weyland.Builder;
using static UnityEngine.UI.Dropdown;
using Debug = UnityEngine.Debug;
using BuilderMesh = UnityEngine.ProBuilder.ProBuilderMesh;
using Weyland.Serializations;
using Weyland.Languages;

namespace Weyland.Builder
{
    public class GenerateCylinder : GenerateMeshBehaviour
    {
        private PivotLocation pivotType = PivotLocation.Center;
        private int axisDivisions = 8;
        private float radius = 0.5f;
        private float height = 1;
        private int heightCuts = 0;
        private int smoothing = -1;

        public override void PreInit()
        {
            string _namespace = this.GetType().FullName;

            pivotType = PlayerData.Read(_namespace + ".pivotType", pivotType);
            axisDivisions = PlayerData.Read(_namespace + ".axisDivisions", axisDivisions);
            radius = PlayerData.Read(_namespace + ".radius", radius);
            height = PlayerData.Read(_namespace + ".height", height);
            heightCuts = PlayerData.Read(_namespace + ".heightCuts", heightCuts);
            smoothing = PlayerData.Read(_namespace + ".smoothing", smoothing);

            GenerateMenu(_namespace);
            MeshGenerate();
        }

        private void GenerateMenu(string _namespace)
        {
            BuilderConfigure.title = Language.Read(_namespace, "Cylinder");

            BuilderConfigure.GenerateEnum(
                 Language.Read("Weyland.Builder.pivotType"),
                 Language.Read("Weyland.Builder.pivotType.info"), typeof(PivotLocation), (int)pivotType).AddListener((int value) =>
                 {
                     pivotType = PlayerData.Write(_namespace + ".pivotLocation", (PivotLocation)value);
                     MeshGenerate();
                 });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".axisDivisions", "Axis Divisions"),
                Language.Read(_namespace + ".axisDivisions.info", "How many divisions to create on the vertical axis.  Larger values = smoother surface."), axisDivisions).AddListener((int value) =>
                {
                    axisDivisions = PlayerData.Write(_namespace + ".axisDivisions", value);
                    MeshGenerate();
                });


            BuilderConfigure.Generate(
                Language.Read(_namespace + ".radius", "Radius"),
                Language.Read(_namespace + ".radius.info", "The radius in world units."), radius).AddListener((float value) =>
                {
                    radius = PlayerData.Write(_namespace + ".radius", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".height", "Height"),
                Language.Read(_namespace + ".height.info", "The height of this object in world units."), height).AddListener((float value) =>
                {
                    height = PlayerData.Write(_namespace + ".height", value);
                    MeshGenerate();
                });
 
 
            BuilderConfigure.Generate(
                Language.Read(_namespace + ".heightCuts", "Height Cuts"),
                Language.Read(_namespace + ".heightCuts.Info", "The amount of divisions to create on the horizontal axis."), heightCuts).AddListener((int value) =>
                {
                    heightCuts = PlayerData.Write(_namespace + ".heightCuts", value);
                    MeshGenerate();
                });
 
            BuilderConfigure.Generate(
                Language.Read(_namespace + ".smoothing"),
                Language.Read(_namespace + ".smoothing.Info"), smoothing).AddListener((int value) =>
                {
                    smoothing = PlayerData.Write(_namespace + ".smoothing", value);
                    MeshGenerate();
                });
        }
        private void MeshGenerate() => Generate(ShapeGenerator.GenerateCylinder(pivotType, axisDivisions, radius, height, heightCuts, smoothing));
    }
}