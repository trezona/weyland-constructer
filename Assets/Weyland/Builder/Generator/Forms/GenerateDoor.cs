﻿// Copyright (c) 2019 All Rights Reserved
// Dmitriy Annenkov
// 03 18 2019

using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.ProBuilder;
using Weyland;
using Weyland.Builder;
using static UnityEngine.UI.Dropdown;
using Debug = UnityEngine.Debug;
using BuilderMesh = UnityEngine.ProBuilder.ProBuilderMesh;
using Weyland.Serializations;
using Weyland.Languages;

namespace Weyland.Builder
{
    public class GenerateDoor : GenerateMeshBehaviour
    {
        private PivotLocation pivotType = PivotLocation.FirstVertex;
        private float totalWidth = 4;
        private float totalHeight = 4;
        private float ledgeHeight = 1;
        private float legWidth = 1;
        private float depth = 0.5f;

        public override void PreInit()
        {
            string _namespace = this.GetType().FullName;

            pivotType = PlayerData.Read(_namespace + ".pivotType", pivotType);
            totalWidth = PlayerData.Read(_namespace + ".totalWidth", totalWidth);
            totalHeight = PlayerData.Read(_namespace + ".totalHeight", totalHeight);
            ledgeHeight = PlayerData.Read(_namespace + ".ledgeHeight", ledgeHeight);
            legWidth = PlayerData.Read(_namespace + ".legWidth", legWidth);
            depth = PlayerData.Read(_namespace + ".depth", depth);

            GenerateMenu(_namespace);
            MeshGenerate();
        }

        private void GenerateMenu(string _namespace)
        {
            BuilderConfigure.title = Language.Read(_namespace, "Door");

            BuilderConfigure.GenerateEnum(
                 Language.Read("Weyland.Builder.pivotType"),
                 Language.Read("Weyland.Builder.pivotType.info"), typeof(PivotLocation), (int)pivotType).AddListener((int value) =>
                 {
                     pivotType = PlayerData.Write(_namespace + ".pivotLocation", (PivotLocation)value);
                     MeshGenerate();
                 });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".totalWidth", "Width"),
                Language.Read(_namespace + ".totalWidth.info", "The total width of the door"), totalWidth).AddListener((float value) =>
                {
                    totalWidth = PlayerData.Write(_namespace + ".totalWidth", value);
                    MeshGenerate();
                });


            BuilderConfigure.Generate(
                Language.Read(_namespace + ".totalHeight", "Height"),
                Language.Read(_namespace + ".totalHeight.Info", "The total height of the door"), totalHeight).AddListener((float value) =>
                {
                    totalHeight = PlayerData.Write(_namespace + ".totalHeight", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".ledgeHeight", "Ledge Height"),
                Language.Read(_namespace + ".ledgeHeight.info", "The height between the top of the door frame and top of the object"), ledgeHeight).AddListener((float value) =>
                {
                    ledgeHeight = PlayerData.Write(_namespace + ".ledgeHeight", value);
                    MeshGenerate();
                });
 
 
            BuilderConfigure.Generate(
                Language.Read(_namespace + ".legWidth", "Ledge Width"),
                Language.Read(_namespace + ".legWidth.info", "The width of each leg on both sides of the door"), legWidth).AddListener((float value) =>
                {
                    legWidth = PlayerData.Write(_namespace + ".legWidth", value);
                    MeshGenerate();
                });
 
            BuilderConfigure.Generate(
                Language.Read(_namespace + ".depth", "Depth"),
                Language.Read(_namespace + ".depth.info", "The distance between the front and back faces of the door object"), depth).AddListener((float value) =>
                {
                    depth = PlayerData.Write(_namespace + ".depth", value);
                    MeshGenerate();
                });
        }
        private void MeshGenerate() => Generate(ShapeGenerator.GenerateDoor(pivotType, totalWidth, totalHeight, ledgeHeight, legWidth, depth));
    }
}