﻿// Copyright (c) 2019 All Rights Reserved
// Dmitriy Annenkov
// 03 18 2019

using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.ProBuilder;
using Weyland;
using Weyland.Builder;
using static UnityEngine.UI.Dropdown;
using Debug = UnityEngine.Debug;
using BuilderMesh = UnityEngine.ProBuilder.ProBuilderMesh;
using Weyland.Serializations;
using Weyland.Languages;

namespace Weyland.Builder
{
    public class GeneratePlane : GenerateMeshBehaviour
    {
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="widthCuts"></param>
        /// <param name="heightCuts"></param>
        /// <param name="axis"></param>

        private PivotLocation pivotType = PivotLocation.Center;
        private float width = 10; 
        private float height = 10;
        private int widthCuts = 0; 
        private int heightCuts = 0;
        private Axis axis = Axis.Up;

        public override void PreInit()
        {
            string _namespace = this.GetType().FullName;

            pivotType = PlayerData.Read(_namespace + ".pivotType", pivotType);
            width = PlayerData.Read(_namespace + ".width", width);
            height = PlayerData.Read(_namespace + ".height", height);
            widthCuts = PlayerData.Read(_namespace + ".widthCuts", widthCuts);
            heightCuts = PlayerData.Read(_namespace + ".heightCuts", heightCuts);
            axis = PlayerData.Read(_namespace + ".axis", axis);

            GenerateMenu(_namespace);
            MeshGenerate();
        }

        private void GenerateMenu(string _namespace)
        {
            BuilderConfigure.title = Language.Read(_namespace, "Plane");

            BuilderConfigure.GenerateEnum(
                 Language.Read("Weyland.Builder.pivotType"),
                 Language.Read("Weyland.Builder.pivotType.info"), typeof(PivotLocation), (int)pivotType).AddListener((int value) =>
                 {
                     pivotType = PlayerData.Write(_namespace + ".pivotLocation", (PivotLocation)value);
                     MeshGenerate();
                 });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".width", "Width"),
                Language.Read(_namespace + ".width.info", "Plane width."), width).AddListener((float value) =>
                {
                    width = PlayerData.Write(_namespace + ".width", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".height", "Height"),
                Language.Read(_namespace + ".height.info", "Plane height."), height).AddListener((float value) =>
                {
                    height = PlayerData.Write(_namespace + ".height", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".widthCuts", "Width Cuts"),
                Language.Read(_namespace + ".widthCuts.info", "Divisions on the X axis."), widthCuts).AddListener((int value) =>
                {
                    widthCuts = PlayerData.Write(_namespace + ".widthCuts", value);
                    MeshGenerate();
                });
 
            BuilderConfigure.Generate(
                Language.Read(_namespace + ".heightCuts", "Height Cuts"),
                Language.Read(_namespace + ".heightCuts.info", "Divisions on the Y axis."), heightCuts).AddListener((int value) =>
                {
                    heightCuts = PlayerData.Write(_namespace + ".heightCuts", value);
                    MeshGenerate();
                });

            BuilderConfigure.GenerateEnum(
            Language.Read(_namespace + ".axis", "Axis"),
            Language.Read(_namespace + ".axis.Info", "The axis to build the plane on. Ex: ProBuilder.Axis.Up is a plane with a normal of Vector3.up."), typeof(Axis), (int)axis).AddListener((int value) =>
            {
                axis = PlayerData.Write(_namespace + ".pivotLocation", (Axis)value);
                MeshGenerate();
            });

        }
        private void MeshGenerate() => Generate(ShapeGenerator.GeneratePlane(pivotType, width, height, widthCuts, heightCuts, axis));
    }
}