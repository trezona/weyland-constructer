﻿// Copyright (c) 2019 All Rights Reserved
// Dmitriy Annenkov
// 03 18 2019

using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.ProBuilder;
using Weyland;
using Weyland.Builder;
using static UnityEngine.UI.Dropdown;
using Debug = UnityEngine.Debug;
using BuilderMesh = UnityEngine.ProBuilder.ProBuilderMesh;
using Weyland.Serializations;
using Weyland.Languages;

namespace Weyland.Builder
{
    public class GenerateCone : GenerateMeshBehaviour
    {
        private PivotLocation pivotType = PivotLocation.FirstVertex;
        private float radius = 1;
        private float height = 2;
        private int subdivAxis = 6;

        public override void PreInit()
        {
            string _namespace = this.GetType().FullName;

            pivotType = PlayerData.Read(_namespace + "Weyland.Builder.pivotType", pivotType);
            radius = PlayerData.Read(_namespace + ".radius", radius);
            height = PlayerData.Read(_namespace + ".height", height);
            subdivAxis = PlayerData.Read(_namespace + ".subdivAxis", subdivAxis);

            GenerateMenu(_namespace);
            MeshGenerate();
        }

        private void GenerateMenu(string _namespace)
        {
            BuilderConfigure.title = Language.Read(_namespace, "Cone");

            BuilderConfigure.GenerateEnum(
                 Language.Read("Weyland.Builder.pivotType"),
                 Language.Read("Weyland.Builder.pivotType.info"), typeof(PivotLocation), (int)pivotType).AddListener((int value) =>
                 {
                     pivotType = PlayerData.Write(_namespace + ".pivotLocation", (PivotLocation)value);
                     MeshGenerate();
                 });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".radius"),
                Language.Read(_namespace + ".radius.info"), radius).AddListener((float value) =>
                {
                    radius = PlayerData.Write(_namespace + ".radius", value);
                    MeshGenerate();
                });

            BuilderConfigure.Generate(
                Language.Read(_namespace + ".height"),
                Language.Read(_namespace + ".height.info"), height).AddListener((float value) =>
                {
                    height = PlayerData.Write(_namespace + ".height", value);
                    MeshGenerate();
                });
 
            BuilderConfigure.Generate(
                Language.Read(_namespace + ".subdivAxis"),
                Language.Read(_namespace + ".subdivAxis.info"), subdivAxis).AddListener((int value) =>
                {
                    subdivAxis = PlayerData.Write(_namespace + ".subdivAxis", value);
                    MeshGenerate();
                });
        }
        private void MeshGenerate() => Generate(ShapeGenerator.GenerateCone(pivotType, radius, height, subdivAxis));
    }
}