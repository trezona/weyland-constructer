﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.ProBuilder;
using UnityEngine.ProBuilder.MeshOperations;

namespace UnityEngine.ProBuilder
{
    public static class eSnapping
    {
        public static Vector3 Ceil(Vector3 vertex, Vector3 snap) => Snapping.Ceil(vertex, snap);
        public static Vector3 Ceil(Vector3 vertex, float snap) => Snapping.Ceil(vertex, snap);

        public static Vector3 Floor(Vector3 vertex, Vector3 snap) => Snapping.Floor(vertex, snap);
        public static Vector3 Floor(Vector3 vertex, float snap) => Snapping.Floor(vertex, snap);

        public static Vector3 Floor(Vector3 normal) => Snapping.GetSnappingMaskBasedOnNormalVector(normal);

        public static Vector3 SnapValue(Vector3 vertex, Vector3 snap) => Snapping.SnapValue(vertex, snap);
        public static Vector3 SnapValue(Vector3 vertex, float snap) => Snapping.SnapValue(vertex, snap);
        public static float SnapValue(float vertex, float snap) => Snapping.SnapValue(vertex, snap);

        public static void SnapValueOnRay(Ray ray, float distance, float snap, object vector3Mask) => Snapping.SnapValueOnRay(ray, distance, snap, (Vector3Mask)vector3Mask);
        public static void SnapValue(ProBuilderMesh mesh, IEnumerable<int> indexes, Vector3 snap) => Snapping.SnapVertices(mesh, indexes, snap);
    }

    public static class eUVEditing
    {
        public static void SetAutoUV(this ProBuilderMesh mesh, IEnumerable<Face> faces, bool auto) => UVEditing.SetAutoUV(mesh, faces.ToArray(), auto);
        
    }
}