﻿using System;
using System.Collections.Generic;
using System.IO;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.ProBuilder;
using Weyland.InventorySystem;
using Weyland.Languages;
using Weyland.Serializations;

namespace Weyland
{
    public class MaterialBuilderChanged : CoreEngine
    {
        private float maxDistance = 5;
        private Material _material;
        private bool _isAutoUnwrapSettings;
        private MaterialDecode _materialSerialization;

        // CACHE PARAM
        private bool _manualUV_facePicker;
        private AutoUnwrapSettings _uv_facePicker;
        private int _submeshIndex_facePicker;
        private ProBuilderMesh _prev_meshPicker;
        private Face _prev_facePicker;

        // HASH OBJECT
        private void Rebuild()
        {
            try
            {
                if (_prev_meshPicker)
                {
                    _prev_facePicker.manualUV = _manualUV_facePicker;
                    _prev_facePicker.uv = _uv_facePicker;
                    _prev_facePicker.submeshIndex = _submeshIndex_facePicker;
                    // if (_prevBuilderMesh != builderMesh)
                    // {
                    _prev_meshPicker.ToMesh();
                    _prev_meshPicker.Refresh();
                    _prev_meshPicker.RefreshUV(new Face[1] { _prev_facePicker });
                    // }
                    _prev_meshPicker = null;
                    _prev_facePicker = null;
                }
            }
            catch (Exception e) { Log.Write(e); }
        }
        private void UpdateMaterial()
        {
            try
            {
                if (_material || _isAutoUnwrapSettings)
                {
                    RaycastHit hitRay;
                    ProBuilderMesh meshPicker = Physics.Raycast(new Ray(Camera.main.transform.position, Camera.main.transform.forward), out hitRay, maxDistance) ? hitRay.transform.GetComponent<ProBuilderMesh>() : null;

                    if (meshPicker != null)
                    {
                        _facePicker = SelectionPicker.PickFace(camera, Input.mousePosition, meshPicker);
                        if (Mouse.current.leftButton.wasReleasedThisFrame && BuilderConfigure.showWindow == false) _prev_meshPicker = null;

                        if (_facePicker != _prev_facePicker)
                        {
                            Rebuild();

                            // SAVE CACHE
                            if ((_prev_facePicker = _facePicker) == null) return;
                            _prev_meshPicker = meshPicker;
                            _submeshIndex_facePicker = _facePicker.submeshIndex;
                            _uv_facePicker = _facePicker.uv;
                            _manualUV_facePicker = _facePicker.manualUV;

                            // EDIT FACE
                            _facePicker.uv = _autoUnwrapSettings;
                            _facePicker.manualUV = PlayerData.Read("Weyland.MaterialBuilderChanged.uv.manualUV", true);
                            if (_material) meshPicker.SetMaterial(new Face[1] { _facePicker }, _material);
                            meshPicker.ToMesh();
                            meshPicker.Refresh();
                        }
                    }
                    else Rebuild();
                }
            }
            catch (Exception e) { Log.Write(e); }
        }

        private Face _facePicker;
        private void Update()
        {
            UpdateMaterial();
            // if (!_material) return;
            // if (Keyboard.current.leftCtrlKey.isPressed && Keyboard.current.cKey.wasPressedThisFrame)
            // {
            //     RaycastHit hitRay;
            //     ProBuilderMesh meshPicker = Physics.Raycast(camera.ScreenPointToCenter(), out hitRay, maxDistance) ? hitRay.transform.GetComponent<ProBuilderMesh>() : null;
            //     if(!meshPicker) return;

            //     InventoryElement.ItemClicked(new InventoryElementAsset
            //     {
            //         name = "Copy Material",
            //         dataObject = _materialSerialization =
            //             new MaterialSerialization(meshPicker.gameObject.GetComponent<MeshRenderer>().sharedMaterials[_submeshIndex_facePicker]),
            //         categoryItem = new InventoryElement
            //         {
            //             name = "Materials"
            //         }
            //     });
            // }
            // if (Keyboard.current.leftCtrlKey.isPressed && Keyboard.current.sKey.wasPressedThisFrame) SaveMaterial(_materialSerialization);
            // if (Keyboard.current.leftCtrlKey.isPressed && Keyboard.current.vKey.wasPressedThisFrame)
            // {
            //     InventoryElement.ItemClicked(new InventoryElementAsset
            //     {
            //         name = "Paste Materials",
            //         dataObject = _materialSerialization,
            //         categoryItem = new InventoryElement
            //         {
            //             name = "Materials"
            //         }
            //     });
            // }
        }

        public Texture tex;
        public void MaterialOnChanged(InventoryElementAsset asset)
        {
            _material = null;
            _isAutoUnwrapSettings = false;
            Rebuild();
            if (asset == null) return;
            try
            {
                if (asset.categoryParent.name == "Materials")
                {
                    BuilderConfigure.Clear();
                    switch (asset.name)
                    {
                        case "UVWrapper":
                            UIWrappedGenerate();
                            _isAutoUnwrapSettings = true;
                            break;
                        case "GenerateMaterial":
                            UIGenerateMaterial(MaterialDecode.Create());
                            break;
                        default:
                            tex = ((asset.ContentData<MaterialDecode>()).baseColorMap.GenerateTexture());
                            BuilderConfigure.title = asset.name;
                            UIGenerateMaterial(asset.ContentData<MaterialDecode>());
                            break;
                    }
                }
            }
            catch (Exception e) { Log.Write(e); }
        }

        private AutoUnwrapSettings _autoUnwrapSettings = new AutoUnwrapSettings();
        private void UIWrappedGenerate()
        {
            try
            {
                BuilderConfigure.title = Language.Read("Weyland.MaterialBuilderChanged.UV", "UV Configuration");
                BuilderConfigure.Generate<object>(Language.Read("Weyland.MaterialBuilderChanged.UV", ""), null, null);

                BuilderConfigure.Generate(
                    Language.Read("Weyland.MaterialBuilderChanged.UV.manualUV", "Manual UV"),
                    Language.Read("Weyland.MaterialBuilderChanged.UV.manualUV.info", ""), PlayerData.Read("Weyland.MaterialBuilderChanged.uv.manualUV", true)).AddListener((bool call) =>
                    {
                        PlayerData.Write("Weyland.MaterialBuilderChanged.uv.manualUV", call);
                        Rebuild();
                    });

                BuilderConfigure.GenerateEnum(Language.Read("Weyland.MaterialBuilderChanged.UV.anchor", "Anchor"),
                Language.Read("Weyland.MaterialBuilderChanged.UV.anchor.info", ""), typeof(AutoUnwrapSettings.Anchor), PlayerData.Read("Weyland.MaterialBuilderChanged.uv.anchor",
                (int)_autoUnwrapSettings.anchor)).AddListener(delegate (int call)
                {
                    _autoUnwrapSettings.anchor = (AutoUnwrapSettings.Anchor)PlayerData.Write("Weyland.MaterialBuilderChanged.uv.anchor", call);
                    Rebuild();
                });

                BuilderConfigure.GenerateEnum(Language.Read("Weyland.MaterialBuilderChanged.UV.fill", "Fill"),
                Language.Read("Weyland.MaterialBuilderChanged.UV.fill.info", ""), typeof(AutoUnwrapSettings.Fill), (int)PlayerData.Read("Weyland.MaterialBuilderChanged.uv.fill",
                _autoUnwrapSettings.fill)).AddListener((int call) =>
                {
                    _autoUnwrapSettings.fill = (AutoUnwrapSettings.Fill)PlayerData.Write("Weyland.MaterialBuilderChanged.uv.fill", call);
                    Rebuild();
                });

                BuilderConfigure.Generate(Language.Read("Weyland.MaterialBuilderChanged.UV.flipU", "Flup U"),
                Language.Read("Weyland.MaterialBuilderChanged.UV.flipU.info", ""), PlayerData.Read("Weyland.MaterialBuilderChanged.uv.flipU",
                _autoUnwrapSettings.flipU)).AddListener((bool call) =>
                {
                    _autoUnwrapSettings.flipU = PlayerData.Write("Weyland.MaterialBuilderChanged.uv.flipU", call);
                    Rebuild();
                });

                BuilderConfigure.Generate(Language.Read("Weyland.MaterialBuilderChanged.UV.flipV", "FlipV"),
                Language.Read("Weyland.MaterialBuilderChanged.UV.flipV.info", ""), PlayerData.Read("Weyland.MaterialBuilderChanged.uv.flipV",
                _autoUnwrapSettings.flipV)).AddListener((bool call) =>
                {
                    _autoUnwrapSettings.flipV = PlayerData.Write("Weyland.MaterialBuilderChanged.uv.flipV", call);
                    Rebuild();
                });

                BuilderConfigure.Generate(Language.Read("Weyland.MaterialBuilderChanged.UV.offset", "Offset"),
                Language.Read("Weyland.MaterialBuilderChanged.UV.offset.info", ""), PlayerData.Read("Weyland.MaterialBuilderChanged.uv.offset",
                (float2)_autoUnwrapSettings.offset)).AddListener(delegate (float2 call)
                {
                    _autoUnwrapSettings.offset = PlayerData.Write("Weyland.MaterialBuilderChanged.uv.offset", call);
                    Rebuild();
                });

                BuilderConfigure.Generate(Language.Read("Weyland.MaterialBuilderChanged.UV.rotation", "Rotation"),
                Language.Read("Weyland.MaterialBuilderChanged.UV.rotation.info", ""), PlayerData.Read("Weyland.MaterialBuilderChanged.uv.rotation",
                _autoUnwrapSettings.rotation)).AddListener((float call) =>
                {
                    _autoUnwrapSettings.rotation = PlayerData.Write("Weyland.MaterialBuilderChanged.uv.rotation", call);
                });

                BuilderConfigure.Generate(Language.Read("Weyland.MaterialBuilderChanged.UV.scale", "Scale"),
                Language.Read("Weyland.MaterialBuilderChanged.UV.scale.info", ""), PlayerData.Read("Weyland.MaterialBuilderChanged.uv.scale",
                (float2)_autoUnwrapSettings.scale)).AddListener(delegate (float2 call)
                {
                    _autoUnwrapSettings.scale = PlayerData.Write("Weyland.MaterialBuilderChanged.uv.scale", call);
                    Rebuild();
                });

                BuilderConfigure.Generate(Language.Read("Weyland.MaterialBuilderChanged.UV.swapUV", "Swap UV"),
                Language.Read("Weyland.MaterialBuilderChanged.UV.swapUV.info", ""), PlayerData.Read("Weyland.MaterialBuilderChanged.uv.swapUV",
                _autoUnwrapSettings.swapUV)).AddListener((bool call) =>
                {
                    _autoUnwrapSettings.swapUV = PlayerData.Write("Weyland.MaterialBuilderChanged.uv.swapUV", call);
                    Rebuild();
                });

                BuilderConfigure.Generate(Language.Read("Weyland.MaterialBuilderChanged.UV.useWorldSpace", "Use World Space"),
                Language.Read("Weyland.MaterialBuilderChanged.UV.useWorldSpace.info", ""), PlayerData.Read("Weyland.MaterialBuilderChanged.uv.useWorldSpace",
                _autoUnwrapSettings.useWorldSpace)).AddListener((bool call) =>
                {
                    _autoUnwrapSettings.useWorldSpace = PlayerData.Write("Weyland.MaterialBuilderChanged.uv.useWorldSpace", call);
                    Rebuild();
                });
            }
            catch (Exception e) { Log.Write(e); }

        }

        private MaterialDecode _rendererSerializationData;
        private void UIGenerateMaterial(MaterialDecode serializationData)
        {
            try
            {
                _material = serializationData.GenerateMaterial();
                BuilderConfigure.title = Language.Read("Weyland.MaterialBuilderChanged.Material.title", "Generate Material");
                BuilderConfigure.Generate<object>(Language.Read("Weyland.MaterialBuilderChanged.Material", "Change paramters for configruration material. Upload texture to directory Data/Textures"), null, null);
                BuilderConfigure.Generate<float4>(
                    Language.Read("Weyland.MaterialBuilderChanged.Material.baseColor", "Base Color"),
                    Language.Read("Weyland.MaterialBuilderChanged.Material.baseColor.info", ""), serializationData.baseColor).AddListener((float4 call) =>
                    {
                        serializationData.baseColor = call;
                        _material = serializationData.GenerateMaterial();
                    });
                BuilderConfigure.GenerateImage(Language.Read("Weyland.MaterialBuilderChanged.Material.baseColorMap", "Albedo Texture"),
                Language.Read("Weyland.MaterialBuilderChanged.Material.baseColorMap.info", ""), Textures.ToArray(), -1, serializationData.baseColorMap.GenerateTexture()).AddListener(delegate (int select, TextureDecode tex)
                {
                    serializationData.baseColorMap = tex;
                    _material = serializationData.GenerateMaterial();
                });
                BuilderConfigure.Generate<float>(Language.Read("Weyland.MaterialBuilderChanged.Material.metallic", "Metallic"),
                Language.Read("Weyland.MaterialBuilderChanged.Material.metallic.info", ""), serializationData.metallic).AddListener((float call) =>
                {
                    serializationData.metallic = call;
                    _material = serializationData.GenerateMaterial();
                });
                BuilderConfigure.Generate<float>(Language.Read("Weyland.MaterialBuilderChanged.Material.smoothness", "Smoothness"),
                Language.Read("Weyland.MaterialBuilderChanged.Material.smoothness.info", ""), serializationData.smoothness).AddListener((float call) =>
                {
                    serializationData.smoothness = call;
                    _material = serializationData.GenerateMaterial();
                });
                BuilderConfigure.GenerateImage(Language.Read("Weyland.MaterialBuilderChanged.Material.maskMap", "Mask"),
                Language.Read("Weyland.MaterialBuilderChanged.Material.maskMap.info", ""), Textures.ToArray()).AddListener(delegate (int select, TextureDecode tex)
                {
                    serializationData.maskMap = tex;
                    _material = serializationData.GenerateMaterial();
                });
                BuilderConfigure.GenerateImage(Language.Read("Weyland.MaterialBuilderChanged.Material.normalMap", "Normal"),
                Language.Read("Weyland.MaterialBuilderChanged.Material.normalMap.info", ""), Textures.ToArray()).AddListener(delegate (int select, TextureDecode tex)
                {
                    serializationData.normalMap = tex;
                    _material = serializationData.GenerateMaterial();
                });
                BuilderConfigure.Generate<float>(Language.Read("Weyland.MaterialBuilderChanged.Material.normalScale", "Normal Scale"),
                Language.Read("Weyland.MaterialBuilderChanged.Material.normalScale.info", ""), serializationData.normalScale).AddListener((float call) =>
                {
                    serializationData.normalScale = call;
                    _material = serializationData.GenerateMaterial();
                });
                BuilderConfigure.GenerateImage(Language.Read("Weyland.MaterialBuilderChanged.Material.bentNormalMap", "Bent Normal"),
                Language.Read("Weyland.MaterialBuilderChanged.Material.bentNormalMap.info", ""), Textures.ToArray()).AddListener(delegate (int select, TextureDecode tex)
                 {
                     serializationData.bentNormalMap = tex;
                     _material = serializationData.GenerateMaterial();
                 });
                BuilderConfigure.GenerateImage(Language.Read("Weyland.MaterialBuilderChanged.Material.heightMap", "Height"),
                Language.Read("Weyland.MaterialBuilderChanged.Material.heightMap.info", ""), Textures.ToArray()).AddListener(delegate (int select, TextureDecode tex)
                 {
                     serializationData.heightMap = tex;
                     _material = serializationData.GenerateMaterial();
                 });
                BuilderConfigure.Generate<float>(Language.Read("Weyland.MaterialBuilderChanged.Material.heightOffset", "Height Offset"),
                Language.Read("Weyland.MaterialBuilderChanged.Material.heightOffset.info", ""), serializationData.smoothness).AddListener((float call) =>
                {
                    serializationData.heightOffset = call;
                    _material = serializationData.GenerateMaterial();
                });
                BuilderConfigure.Generate<string>(Language.Read("Weyland.MaterialBuilderChanged.Material.nameMaterial", "Name Material"),
                Language.Read("Weyland.MaterialBuilderChanged.Material.nameMaterial.info", "this name material created a directory Data/Materials"), serializationData.nameMaterial).AddListener((string call) =>
                {
                    serializationData.nameMaterial = call;
                    _material = serializationData.GenerateMaterial();
                });

                BuilderConfigure.GenerateButton(Language.Read("Weyland.MaterialBuilderChanged.Material.saveMaterial", "Save Material")).AddListener(() =>
                {
                    _material = serializationData.GenerateMaterial();
                    SaveMaterial(serializationData);

                });
                BuilderConfigure.Generate<object>(Language.Read("Weyland.MaterialBuilderChanged.Material.saveInfo", ""), null, null);
            }
            catch (Exception e) { Log.Write(e); }
        }

        // LOAD PLAYER TEXTURE
        public static List<TextureDecode> Textures = new List<TextureDecode>();
        public static List<TextureDecode> ReadTextureDirectory()
        {
            if (!Directory.Exists($"{Application.dataPath}/Data/Textures/")) return new List<TextureDecode>();
            List<TextureDecode> _arrayTexture = new List<TextureDecode>();

            TextureDecode textureSerialization;
            int index = -1;
            foreach (var file in Directory.GetFiles($"{Application.dataPath}/Data/Textures/", "*.??g", SearchOption.AllDirectories))
            {
                string[] fileNameArray;
                string fileName = (fileNameArray = file.Split('/'))[fileNameArray.Length - 1].Split('.')[0];

                textureSerialization = TextureDecode.CreatePNG(File.ReadAllBytes(file));
                if ((index = Textures.FindIndex(u => u.byteTexture == textureSerialization.byteTexture)) > -1) continue;
                _arrayTexture.Add(textureSerialization);

                // Inventory.CreateCategory("TEST").CreateElement(new InventoryElementAsset
                // {
                //     name = textureSerialization.nameTexture,
                //     textureImage = textureSerialization.GenerateTexture()
                // });
            }
            Textures.AddRange(_arrayTexture);
            return _arrayTexture;
        }

        // LOAD CORE / PLAYER MATERIALS
        public static List<MaterialDecode> ResourceMaterials = new List<MaterialDecode>();
        public static List<MaterialDecode> Materials = new List<MaterialDecode>();
        public static List<MaterialDecode> ReadMaterialDirectory()
        {
            // LOAD SYSTEM MATERIALS
            try
            {
                ResourceMaterials.Clear();
                foreach (Material mat in MaterialDecode.GetResources())
                {
                    MaterialDecode materialSerialization = MaterialDecode.Create(mat);

                    Material spriteMaterial = new Material(Shader.Find("Sprites/Default"));
                    spriteMaterial.SetTexture("_MainTex", mat.GetTexture("_BaseColorMap"));

                    Inventory.CreateCategory("Materials").CreateElement(new InventoryElementAsset(mat.name, "Materials", materialSerialization, spriteMaterial));
                    ResourceMaterials.Add(materialSerialization);
                }
            }
            catch (Exception e) { Log.Write(e); }

            try
            {

                if (!Directory.Exists($"{Application.dataPath}/Data/Materials/")) return Materials;
                List<MaterialDecode> _array = new List<MaterialDecode>();
                foreach (var fileName in Directory.GetFiles($"{Application.dataPath}/Data/Materials/", "*.json"))
                {
                    string[] fileNameArray;
                    string file = (fileNameArray = fileName.Split('/'))[fileNameArray.Length - 1].Split('.')[0];
                    if (Materials.FindIndex(u => u.nameMaterial == file) > -1) continue;

                    var prefab = MaterialDecode.Create(File.ReadAllText(fileName));
                    _array.Add(prefab);
                    Inventory.CreateCategory("Materials").CreateElement(new InventoryElementAsset(prefab.nameMaterial, "Materials", prefab, prefab.baseColorMap.GenerateTexture()));
                }
                Materials.AddRange(_array);
                return _array;
            }
            catch (Exception e) { Log.Write(e); }
            return null;
        }

        private bool SaveMaterial(MaterialDecode materialSerialization)
        {
            try
            {
                Directory.CreateDirectory($"{Application.dataPath}/Data/Materials/");
                using (FileStream fstream = new FileStream($"{Application.dataPath}/Data/Materials/{materialSerialization.nameMaterial}.json", FileMode.OpenOrCreate))
                {
                    byte[] array = System.Text.Encoding.Default.GetBytes(materialSerialization.ToJson());
                    fstream.Write(array, 0, array.Length);
                    fstream.Close();
                }
                // copyMesh.GetComponent<MeshRenderer>().sharedMaterials.Save($"Data/Models/{Time.time}", "Material");
                ReadMaterialDirectory();
            }
            catch (Exception e) { Log.Write(e); }

            return true;
        }
        private static Texture Texture(byte[] imageBytes)
        {
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(imageBytes);
            return texture;
        }

        private void OnEnable()
        {
            Inventory.elementOnChanged.AddListener(MaterialOnChanged);
            ReadTextureDirectory();
        }
    }
}