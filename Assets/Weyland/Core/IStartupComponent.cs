namespace Weyland
{
    public interface IStartupComponent
    {
        void Add<T>() where T : IStartup, new();
        void Remove<T>() where T : IStartup;
        void Remove(IStartup startup);
        void Clear();
    }
}