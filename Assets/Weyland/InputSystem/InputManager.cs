using UnityEngine;
using static Weyland.InputSystem.InputController;
using UnityEngine.InputSystem;
using Weyland.InputSystem;
using Unity.Mathematics;

namespace Weyland
{
    [DisallowMultipleComponent]
    public class InputManager : MonoBehaviour
    {
        private static InputController _inputController;

        public static InputController Controller { get => _inputController; }
        public static PlayerActions Player { get => _inputController.Player; }
        public static ToolbarActions Toolbar { get => _inputController.Toolbar; }
        // public static Menus MenuKeys { get => _inputController; }
        public static bool activeMouse 
        {
            get => (Cursor.lockState != CursorLockMode.Locked && Cursor.visible == true);
            set 
            { 
                Cursor.lockState = (Cursor.visible = value) ? CursorLockMode.None : CursorLockMode.Locked;
            }
        }

        private void Update()
        {
            if (Keyboard.current.tabKey.wasReleasedThisFrame)
            {
                if (InputManager.activeMouse = !InputManager.activeMouse) InputManager.Player.MouseDelta.Disable();
                else InputManager.Player.MouseDelta.Enable();
            }
        }
        private void Start()
        {
            // Debug.Log(Controller.Player.Get().actions[0].ChangeBinding(0).);
        }
        private void Awake() => Install();
        private void Install(InputActionAsset asset = null)
        {
            _inputController = new InputController();
            // if(asset != null) _inputController.SetPrivateField("asset", asset);
        }


        public static bool mouseLeftPressed => Mouse.current.leftButton.wasPressedThisFrame;
        public static bool mouseLeftReleased => Mouse.current.leftButton.wasReleasedThisFrame;
    }
}