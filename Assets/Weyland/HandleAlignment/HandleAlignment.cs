﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Weyland
{
    [DisallowMultipleComponent]
    public class HandleAlignment : MonoBehaviour
    {
        private void SetModeAlignment(AlignmentEnum alignmentEnum)
        {
            for (int i = 0; i < _arrayAlignmentObjects.Length; i++) 
            {
                _arrayAlignmentObjects[i].SetActive(i == (int)alignmentEnum);
            }
            _modeAlignment = alignmentEnum;
        }
        private AlignmentEnum _modeAlignment = AlignmentEnum.Global;
        public static AlignmentEnum modeAlignment
        {
            get => _instance._modeAlignment;
            set => _instance.SetModeAlignment(value);
        }

        private GameObject _snapGameObject;
        public static bool snapActive 
        {
            get => _instance._snapGameObject.activeSelf;
            set => _instance._snapGameObject.SetActive(!snapActive);
        }

        private Text _textMessage;
        public static string textMessage 
        {
            get => _instance._textMessage.text;
            set
            {
                _instance.textActive = !string.IsNullOrEmpty(value);
                _instance._textMessage.text = value;
            }
        }
        private bool textActive
        {
            get => _textMessage.gameObject.activeSelf;
            set => _textMessage.gameObject.SetActive(value);
        }
        
        private GameObject[] _arrayAlignmentObjects = new GameObject[4];
        private void Awake()
        {
            _instance = this;
            _arrayAlignmentObjects[(int)AlignmentEnum.Custom] = transform.Find("Custom").gameObject;
            _arrayAlignmentObjects[(int)AlignmentEnum.Global] = transform.Find("Global").gameObject;
            _arrayAlignmentObjects[(int)AlignmentEnum.Local] = transform.Find("Local").gameObject;
            _arrayAlignmentObjects[(int)AlignmentEnum.Plane] = transform.Find("Plane").gameObject;
            modeAlignment = modeAlignment;

            //SNAP ENABLE
            _snapGameObject = transform.Find("Snap").gameObject;
            _textMessage = transform.Find("Text").GetComponent<Text>();

            //SETUP CONFIG
            textActive = false;
        }
        private void Update() 
        {
            if(Keyboard.current.pKey.wasReleasedThisFrame) Next();
            if(Keyboard.current.qKey.wasReleasedThisFrame) snapActive = !snapActive;
        }

        public static void Next()
        {
            switch ((int)modeAlignment)
            {
                case 3: modeAlignment = AlignmentEnum.Global; break; // case 3: modeAlignment = AlignmentEnum.Custom; break;
                default: modeAlignment = modeAlignment + 1; break;
            }
        }
    
        public enum AlignmentEnum
        {
            Custom = 0,
            Global = 1,
            Local = 2,
            Plane = 3,
        }

        private static HandleAlignment _instance;// { get; private set; }
    }
}